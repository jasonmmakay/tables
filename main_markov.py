from src.debug import enable_log
from src.markov import MarkovChain, Start, End

import numpy as np

import argparse
import collections
import glob
import math
import os
import pickle
import random
import re
import time


def printf(msg, *args, **kwargs):
    """Simple wrapper for print(msg.format(*args, **kwargs))."""
    print(msg.format(*args, **kwargs))


def expand_globs(patterns):
    """
    Expand any glob exrepssions in file names.

    Anything after an equal sign is treated as a label for each file expanded
    from a pattern and is separated from the pattern and then applied to each
    expanded file: *.txt=X -> a.txt=X b.txt=X c.txt=X
    """
    files = []
    for pattern in patterns:
        # handle labels/mappings/properties in the patterns
        label = None
        if '=' in pattern:
            pattern, label = pattern.split('=', 1)

        # expand the pattern
        new_files = glob.iglob(pattern)

        # add the label to each file if there was one
        if label is not None:
            new_files = (f'{file}={label}' for file in new_files)

        files.extend(new_files)

    return files


def print_stats(stats, label):
    """
    Print statistics about seqeunces.

    Display the number of sequences, average length and histogram of lengths.
    """
    print(label)

    n = sum(stats.values())
    avg = sum(length * count for length, count in stats.items()) / n
    print(' {} sequences'.format(n))
    print(' {:.1f} average length'.format(avg))

    max_n = max(stats.keys())
    width_n = calc_width(max_n)
    max_c = max(stats.values())
    width_c = calc_width(max_c)

    fmt = '{{:{}}}  {{:{}}}  {{}}'.format(width_n, width_c)

    for n in range(max_n + 1):
        c = stats.get(n, 0)
        c0 = int(round(c / max_c * 20))
        print(fmt.format(n, c, '|' * c0))

    print()


def stat_words(args):
    """Read sequences from files and print statistics about them."""
    stats = collections.Counter()
    for file in args.files:
        file_stats = collections.Counter()

        for seq in read_training_sequence_file(file):
            file_stats[len(seq)] += 1

        stats.update(file_stats)
        if args.each:
            print_stats(file_stats, file)

    print_stats(stats, 'OVERALL')


def calc_width(n):
    """Calculate the number of characters needed to display an integer."""
    if n > 0:
        return int(math.log10(n) + 1)
    elif n < 0:
        # +1 extra for sign
        return int(math.log10(abs(n)) + 2)
    else:
        # log(0) is undefined, but 0 is one chnaracter
        return 1


def stat_chains(args):
    """
    Print statistics about a group of markov chains.

    Find the subsequences from a group of markov chains that are not only a
    starting or ending subsequence (ie, it appears in the middle of at least
    one sequence) in at least two of the markov chains.

    Print all such subsequences with their counts from all markov chains,
    arranged in indexed columns corresponding to each chain, and a T column
    with the sum of those counts.
    """
    # figure out how wide the index is
    w = calc_width(len(args.files))

    # print the index for each file and the file name and load them
    ms = []
    for idx, f in enumerate(args.files):
        printf('{:{w}} {}'.format(idx, f, w=w))
        ms.append(MarkovChain.load(f))
        assert ms[0].order == ms[-1].order

    # spacer
    print()

    # find all of the subsequences that occur in the middle of other sequences
    # ie, those that do not have the Start or End symbols and transition to
    # some other symbol than just Start or End
    valid_subs = collections.Counter()
    for m in ms:
        for sub, trans in m.chains.items():
            # don't count subsequences that have a Start or End symbol in them
            if sub[0] is Start or sub[-1] is End:
                continue

            # or those that only transition to End symbol
            if not trans or End in trans and len(trans) == 1:
                continue

            # or those that can only be generated from the Start symbol
            trans = m.back_chains.get(sub)
            if not trans or Start in trans and len(trans) == 1:
                continue

            valid_subs[sub] += 1

    # of the valid subsequences, count how many observations of each of them
    # are in each of the markov chains, then filter out those that only appear
    # in a single chain. for those that we keep, figure out the width of the
    # column we will need to display the counts.
    sub_counts = []
    widths = [1] * (len(ms) + 1)
    for sub, count in valid_subs.items():
        # if the subsequence was only in one chain, skip it
        if count < 2:
            continue

        # count the number of observations of the subsequence in each chain and
        # reserve a spot for thier sum
        count = [0, *(m.count(sub) for m in ms)]

        # update the widths and calculate the sum of observations
        for idx in range(len(ms)):
            widths[idx + 1] = max(widths[idx + 1], calc_width(count[idx + 1]))
            count[0] += count[idx + 1]

        # update the width of the sum
        widths[0] = max(widths[0], calc_width(count[0]))

        # store the result
        sub_counts.append((''.join(sub), *count))

    if not sub_counts:
        # no sequences were found
        print('no common subseqeunces')

    else:
        # sort the subsequences and counts by its total observation count, in
        # descending order
        sub_counts.sort(key=lambda x: x[1], reverse=True)

        # build a format string from the widths we calculate above
        fmt = (
            '{{:>{w}}}  '.format(w=ms[0].order)
            + '  '.join('{{:>{w}}}'.format(w=w) for w in widths))

        # print a header for the table
        printf(fmt, '-', 'T', *range(len(ms)))

        # and, finally, the subsequences and counts from each markov chain
        for sub_and_count in sub_counts:
            printf(fmt, *sub_and_count)


def read_stat_chains_file(file):
    """Read the output of stat_chains from file."""
    with open(file) as f:
        order = 0
        for line in f:
            match = re.search(r'^(\s*-)  \s*T', line)
            if match:
                order = len(match.group(1))
                break

        assert order
        for line in f:
            sub = line[:order]
            line = line[order:].strip()
            counts = (int(x) for x in line[order:].split())
            yield (sub, *counts)


def stat_sequence(args):
    """Print odds of generating a sequence from a markov chain."""
    w = calc_width(len(args.files))
    ms = []
    for idx, file in enumerate(args.files):
        printf('{:{w}} {}', idx, file, w=w)
        ms.append(MarkovChain.load(file))

    # spacer
    print()

    base_fmt = (
        # index and total probability
        '{{:{w}}}  {{:{t}}}  '
        # probability of each character
        + '  '.join('{{:{t}}}' for x in args.sequence))

    header_fmt = base_fmt.format(w=w, t='>8')
    row_fmt = base_fmt.format(w=w, t='.2e')

    printf(header_fmt, '', 'TOTAL', *args.sequence, w=w)

    for idx, m in enumerate(ms):
        p = [float(p0) for p0 in m.odds(args.sequence)]
        p1 = math.prod(p)
        printf(row_fmt, idx, p1, *p, w=w)


def timed(msg, f):
    """
    Run a parameterless function and display how long it takes to complete.

    PARAMETERS
    ----------
    msg - Message to display before running the function.
    f - The function to run.

    RETURNS
    -------
    The result from f().
    """
    print(msg, end=' ', flush=True)
    start = time.time_ns()
    r = f()

    print(
        'done in {:.0f}s'
        .format((time.time_ns() - start) * 1e-9),
        flush=True)

    return r


def load_or_create_cache(ctor, cache, recache, *dependencies):
    """
    Load an object from cache or build an object and save it.

    PARAMETERS
    ----------
    ctor - A parameterless function to build the object if it was not loaded.
    cache - The path to the cache file.
    recache - Ignore the existence of a cache and regenerate the object.
    *dependencies - Paths to files that the cache object depends on; if the
        cache file exists but any of the dependency files are newer than the
        cache, the object is rebuilt and the cache updated.
    """
    # figure out if the cache can be loaded
    load_cache = False
    if not recache and os.path.exists(cache):
        load_cache = True
        cache_time = os.path.getmtime(cache)
        for dependency in dependencies:
            if os.path.getmtime(dependency) > cache_time:
                load_cache = False
                break

    # load the cache if possible
    if load_cache:
        with open(cache, 'rb') as f:
            return pickle.load(f)

    # the cache wasn't loaded, build the object and save it
    obj = ctor()
    with open(cache, 'wb') as f:
        pickle.dump(obj, f)

    return obj


def calc_markov_stats(args, m):
    """
    Calculate transition probabilities for a markov chain.

    RETURNS
    -------
    ids - The mapping of tuples from the markov chain to indices in the output
        matrices.
    h - A matrix containing the probability of seeing a sequence (dst) given a
        preceding seqeunce (src)
    """
    # all sequences have the same starting and ending symbols and since all
    # sequences in the chain end at the same symbol, there is ony one absorbing
    # state in the chain, the end symbol here.
    start = (Start,) * m.order
    end = (End,) * m.order
    absorb = (*end[:-1], None)

    assert start in m.chains
    assert end in m.chains
    assert absorb not in m.chains
    assert {None} == {*m.chains[end].keys()}

    # choose the id for the absorbing state to be the id after all others
    ids = {absorb: len(m.chains)}

    # assign ids for all of the other states
    for seq in m.chains.keys():
        # -1 to account for the absorbing state that was already assigned
        ids[seq] = len(ids) - 1

    assert len(ids) == len(m.chains) + 1

    # construct the transition probability matrix, of moving from one state to
    # another in a single step
    print('copying chains', flush=True)
    p = np.zeros((len(ids), len(ids)))
    for current, transitions in m.chains.items():
        current_id = ids[current]
        total = sum(transitions.values())
        for character, weight in transitions.items():
            next_id = ids[(*current[1:], character)]
            p[current_id, next_id] = weight / total

    # https://en.wikipedia.org/wiki/Absorbing_Markov_chain#Fundamental_matrix
    q = p[:-1, :-1]
    r = p[:-1, -1:]

    # calculate the probabilities of seeing some state in a sequence given some
    # starting state
    i = np.identity(len(ids) - 1)
    n = timed('calculating n', lambda: np.linalg.inv(i - q))
    ndi = timed('calculating ndi', lambda: np.linalg.inv(np.diag(np.diag(n))))
    h = timed('calculating h', lambda: np.matmul(n - i, ndi))
    b = timed('calculating b', lambda: np.matmul(n, r))

    return ids, h, b


def stat_subsequences(args):
    """Calculate the odds of generating subsequences from a markov chain."""
    m = MarkovChain.load(args.file)

    subs = {}
    for sub in args.subsequences:
        subs.setdefault(sub, len(subs))

    for file in args.files:
        for sub_and_counts in read_stat_chains_file(file):
            subs.setdefault(sub_and_counts[0], len(subs))

    subs = [
        idx_and_sub[-1] for idx_and_sub in
        sorted((idx, sub) for sub, idx in subs.items())]

    assert all(len(sub) == m.order for sub in subs)

    ids, h, b = load_or_create_cache(
        lambda: calc_markov_stats(args, m),
        '{}.stats.cache'.format(args.file),
        args.recache,
        args.file)

    start_id = ids.get((Start,) * m.order)
    assert start_id is not None

    def char_to_symbol(x):
        if x == '<':
            return Start
        elif x == '>':
            return End
        else:
            return x

    # calculate the probability of seeing any of the subsequences in a newly
    # generated sequence
    p_none = 1

    for sub in subs:
        sub_key = tuple(char_to_symbol(x) for x in sub)

        p = 0
        sub_id = ids.get(sub_key)
        if sub_id is not None:
            p = h[start_id, sub_id]

        # the probability of not seeing any of the subsequences in a generated
        # sequence is the product of the probabilities for not seeing any one
        # of them, one minus the probability of seeing the subsequence
        p_none *= (1 - p)
        printf('{} {:.2e}', sub, p)

    # spacer
    print()

    # the probability of seeing at least one of the subseqeunces in a generated
    # sequence is one minus the probability of seeing none of them
    printf('any {:.2e}', 1 - p_none)


def parse_training_sequence(line):
    """
    Parse a line from a training sequence file.

    Handle
    * comment lines, starting with #, return None,
    * lines starting with ##, which escapes the comment character, strip first,
    * otherwise, return the line unchnaged.
    """
    line = line.strip()
    if len(line) == 0:
        return None
    elif line.startswith('##'):
        return line[1:]
    elif line.startswith('#'):
        return None
    else:
        return line


def read_training_sequence_file(file):
    """
    Yield training sequnces, one per line, from a text file.

    Empty and comment lines are discarded.
    """
    with open(file) as f:
        for line in f:
            # handle special parsing of the line
            line = parse_training_sequence(line)

            # skip empty lines
            if line:
                yield line


def train_markov(args):
    """Train a markov chain."""
    m = MarkovChain([], args.output, order=args.order)
    for spec in args.files:
        if '=' in spec:
            file, weight = spec.split('=', 1)
            weight = int(weight)
        else:
            file, weight = spec, 1

        print('reading "{}" weight {}'.format(file, weight))

        # zero weight means nothing gets added, don't even read the file
        if weight == 0:
            continue

        sequences = [x for x in read_training_sequence_file(file)]
        m.add(sequences, weight)

    print('saving {}'.format(args.output))
    m.save(args.output)


def upgrade_markov(args):
    """Upgrade all markov chains to the latest code version."""
    for file in args.files:
        print('upgrading {}'.format(file))
        MarkovChain.load(file, upgrade_warning=False).save(file)


def generate_markov(args):
    """Generate strings from a markov chain."""
    print('loading {}'.format(args.file))
    m = MarkovChain.load(args.file)

    random.seed(args.seed)
    print('seed:', args.seed)

    count = 0
    result_set = set()
    while count < args.count:
        result = m.generate(
            front=args.front,
            middle=args.middle,
            back=args.back)

        # for unique sequence resqusts, it can take some time between when a
        # new sequence is found, so flush stdout to keep the display updated.
        # otherwise, we're probably pumping the buffer fast anough to keep
        # things moving on the display since we will print every sequence.
        if args.unique:
            if result not in result_set:
                count += 1
                result_set.add(result)
                print(result, flush=True)

        elif args.try_unique:
            count += 1
            if result not in result_set:
                result_set.add(result)
                print(result, flush=True)

        else:
            count += 1
            print(result)


def add_parser(subparsers, *args, **kwargs):
    """
    Create a subparser.

    If either of the 'help' or 'description' options is not set, but the other
    is, copy one to the other, duplicating the message.
    """
    if 'help' in kwargs and 'description' not in kwargs:
        kwargs['description'] = kwargs['help']
    elif 'description' in kwargs and 'help' not in kwargs:
        kwargs['help'] = kwargs['description']

    return subparsers.add_parser(*args, **kwargs)


def main():
    """Parse the command line and then complete the specified action."""
    # keep track of which subparser group we are currently using
    subparsers_stack = []

    main_parser = argparse.ArgumentParser()
    main_parser.add_argument(
        '--log', action='store_true',
        help='Enable logging to log.txt')
    main_parser.add_argument(
        '--recache', action='store_true',
        help='Ignore the existence of a cache and regenerate it.')

    subparsers = main_parser.add_subparsers(
        dest='command', required=True)
    subparsers_stack.append(subparsers)

    ###########################################################################
    parser = add_parser(
        subparsers,
        'train',
        help='Train a new markov chain.')

    parser.add_argument(
        'files', nargs='+', metavar='TXT[=WEIGHT]',
        help='''
            Text files containing a sequence per line to use for training the
            markov chain; endline characters are stripped off. If the argument
            contains an equal sign, the left side is read as the file name and
            the right side is the weight to apply for each sequence in the
            file; when not specified, this weight defaults to 1.
            ''')
    parser.add_argument(
        'output', metavar='MARKOV',
        help='Location to write the pickled markov chain object to.')
    parser.add_argument(
        '--order', default=1, type=int,
        help='''
            Number of past symbols to consider when choosing the next symbol in
            a sequence; default=%(default)s.
            ''')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'upgrade',
        help='''
            Convert an outdated markov file to the newest object format.  In
            most cases, upgrades are functional and should not impact sequence
            generation repeatability.
            ''')
    parser.add_argument(
        'files', metavar='MARKOV', nargs='+',
        help='The pickled markov chain objects to upgrade, in-place.')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'generate',
        help='Generate sequences from a markov chain.')

    parser.add_argument(
        'file', metavar='MARKOV',
        help='The pickled markov chain object to generate sequences from.')
    parser.add_argument(
        'count', metavar='COUNT', nargs='?', default=1, type=int,
        help='The number of sequences to generate; default=%(default)s.')
    parser.add_argument(
        '--seed', type=int, default=time.time_ns(),
        help='Random number generator seed.')

    mutex_group = parser.add_mutually_exclusive_group()
    mutex_group.add_argument(
        '--unique', action='store_true',
        help='''
            Generate random sequences until exactly COUNT unique sequences are
            created. Output is flushed immediately, but COUNT can be set so
            high that the number of requested sequences are never able to be
            generated and the application never exits.
            ''')
    mutex_group.add_argument(
        '--try-unique', action='store_true',
        help='Make COUNT attempts to generate unique sequences.')

    mutex_group = parser.add_mutually_exclusive_group()
    mutex_group.add_argument(
        '--front',
        help='Generate all sequences with the same beginning subsequence.')
    mutex_group.add_argument(
        '--middle',
        help='Generate all sequences with a common middle subsequence.')
    mutex_group.add_argument(
        '--back',
        help='Generate all sequences with the same ending subsequence.')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'stat',
        help='Calculate statistics for sets of words and markov chains.')
    subparsers = parser.add_subparsers(
        dest='stat_command', required=True)
    subparsers_stack.append(subparsers)

    ###########################################################################
    parser = add_parser(
        subparsers,
        'words',
        help='''
            Calculate statistics about sequences in text files: the number of
            sequences, average length of seqeunces and histogram of sequence
            lengths.
        ''')
    parser.add_argument(
        'files', nargs='+', metavar='TXT',
        help='''
            Text files containing a sequence per line to generate stats from,
            including: number of sequences, average sequence length, and a
            histogram of sequence lengths.
            ''')
    parser.add_argument(
        '--each', action='store_true',
        help='Display stats for each file in addition to overall stats.')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'chains',
        help='''
            Find subsequences that occur in at least two markov chains that
            also occur in the middle of sequences. Show the total occurrences
            across all chains and the number of occurrences in each chain in a
            fixed-width table.
            ''')
    parser.add_argument(
        'files', nargs='+', metavar='MARKOV',
        help='The markov chains to examine.')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'seq',
        help='''
            Calculate the probability of generating a sequence from one or more
            markov chains.
            ''')
    parser.add_argument(
        'files', metavar='MARKOV', nargs='+',
        help='The markov chains to calculate odds of generation from.')
    parser.add_argument(
        'sequence',
        help='The sequence to calculate odds of generation for.')

    ###########################################################################
    parser = add_parser(
        subparsers,
        'subs',
        help='''
            Calculate the probability that each subsequence would appear in a
            seqeunce generated from a markov chain and the probability that a
            generated sequence would contain at least one of the subsequences.
            ''')
    parser.add_argument(
        'file', metavar='MARKOV',
        help='The markov chain to examine.')
    parser.add_argument(
        'subsequences', metavar='SUBSEQUENCE', nargs='*', default=[],
        help='Subsequences to calculate probabilities for.')
    parser.add_argument(
        '--stats', metavar='STATS', dest='files', nargs='*', default=[],
        help='''
            Output from `markov.py stat chains` saved to file; the subsequences
            are read and the counts are ignored.
            ''')

    subparsers_stack.pop()  # stat_command
    subparsers_stack.pop()  # command

    args = main_parser.parse_args()

    # turn on logging if it was requested
    if args.log:
        enable_log('markov')

    # expand glob expressions
    if 'files' in args:
        args.files = expand_globs(args.files)

    # run the command
    if args.command == 'train':
        train_markov(args)
    elif args.command == 'upgrade':
        upgrade_markov(args)
    elif args.command == 'generate':
        generate_markov(args)
    elif args.command == 'stat':
        if args.stat_command == 'words':
            stat_words(args)
        elif args.stat_command == 'chains':
            stat_chains(args)
        elif args.stat_command == 'seq':
            stat_sequence(args)
        elif args.stat_command == 'subs':
            stat_subsequences(args)


if __name__ == '__main__':
    main()
