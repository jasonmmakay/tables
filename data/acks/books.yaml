# MACROS #####################################################################
scope:              '{table-scope}'
complexity:         '{complexity-by-scope-{scope}}'
complexity-label:   '{complexity-labels[{complexity}]}'
pages:              '{@[{scope}*1000//{complexity-label}]}' # Yes, the label!
reading-time:       '{@[({pages}+90)//18/10]}'
volumes:            '{@[ceil({pages}/{pages-per-volume-by-format-{format}})]}'
language:           '{table-language}'
age:                '{age-by-language-{language}}'
age-label:          '{book-age-labels[{age}]}'
format:             '{format-by-age-{age}}'
material:           '{material-by-format-{format}}'
material-label:     '{material-labels[{material}]}'
weight-per-page:    '{weight-by-material-labels[{material}]}'
weight:             '{@[{weight-per-page}*{pages}]}'
prod-per-page:      '{prices-by-format-{format}-labels[{material}]}'
production-value:   '{@[{prod-per-page}*{pages}/100]:.2f}'
rarity:             '{number-of-extant-copies}'
rarity-label:       '{number-of-copies-labels[{rarity}]}'
literary-mod:       '{rarity-modifier-labels[{rarity}]}'
literary-per-page:  '{literary-value-by-scope-and-complexity-labels[{scope}][{complexity}]}'
literary-value:     '{@[{literary-mod}*{literary-per-page}*{pages}]:.2f}'
total-value:        '{@[floor({production-value}+{literary-value}+0.5)]}'

# Labels #####################################################################

complexity-labels: [0.75, 1, 2, 3, 4, 5, 6, 7]

book-age-labels:
- 10 years or newer  # 0
- 11-30 years        # 1
- 31-50 years        # 2
- 51-100 years       # 3
- 101-500 years      # 4
- 501-1000 years     # 5
- 1000-2000 years    # 6
- 2000 years or more # 7

number-of-copies-labels:
- Unique       # 0
- 2 to 5       # 1
- 6 to 25      # 2
- 26 to 50     # 3
- 51 to 100    # 4
- 101 to 250   # 5
- 251 to 500   # 6
- 501 to 1000  # 7
- 1000 or more # 8

rarity-modifier-labels: [1.00, 0.50, 0.33, 0.25, 0.10, 0.05, 0.02, 0.01, 0.00]

literary-value-by-scope-and-complexity-labels:
- [] # No Scope 0 books
- [ 1.0,  1.0,  1.25,  2.0,  5.0]
- [ 2.0,  2.0,  2.0,   2.5,  4.0, 10.0]
- [ 4.0,  4.0,  4.0,   4.0,  5.0,  8.0, 20.0]
- [10.0, 10.0, 10.0,  10.0, 10.0, 12.5, 20.0, 50.0]


material-labels:
- Parchment    # 0
- Vellum       # 1
- Copper Foil  # 2
- Silver Foil  # 3
- Gold Foil    # 4
- Papyrus      # 5
- Fine Papyrus # 6
- Clay Tablet  # 7
- Stone Tablet # 8

prices-by-format-Codex-labels: [37, 39, 43, 181, 3031]
prices-by-format-Scroll-labels: [36, 38, 42, 180, 3030, 31, 33]
prices-by-format-Tablet-labels: [0, 0, 0, 0, 0, 0, 0, 80, 130]

weight-by-material-labels: [0.06, 0.06, 0.12, 0.15, 0.30, 0.06, 0.06, 0.75, 1.00]

pages-per-volume-by-format-Codex:  750
pages-per-volume-by-format-Scroll: 250
pages-per-volume-by-format-Tablet: 1

# Weighted Tables ############################################################

table-scope-table:
  1: 55
  2: 30
  3: 15
  4: 5

complexity-by-scope-1-table:
  0: 25
  1: 25
  2: 41
  3: 8
  4: 1

complexity-by-scope-2-table:
  0: 6
  1: 14
  2: 40
  3: 30
  4: 8
  5: 1

complexity-by-scope-3-table:
  0: 1
  1: 5
  2: 14
  3: 40
  4: 30
  5: 8
  6: 1

complexity-by-scope-4-table:
  0: 1
  1: 5
  2: 5
  3: 15
  4: 33
  5: 32
  6: 8
  7: 1

table-language-table:
  Classical:  30
  Common:     35
  Regional:   20
  Dwarven:    5
  Elven:      5
  Ancient:    5

age-by-language-Classical-table:
  0:  10
  1:  10
  2:  15
  3:  40
  4:  20
  5:  5

age-by-language-Common-table:
  0:  5
  1:  25
  2:  50
  3:  20
  4:  5

age-by-language-Regional-table:
  0:  5
  1:  20
  2:  50
  3:  25

age-by-language-Dwarven-table:
  0:  5
  1:  10
  2:  15
  3:  20
  4:  25
  5:  20
  6:  4
  7:  1

age-by-language-Elven-table:
  0:  5
  1:  5
  2:  10
  3:  20
  4:  20
  5:  20
  6:  15
  7:  5

age-by-language-Ancient-table:
  0:  5
  1:  5
  2:  5
  3:  5
  4:  10
  5:  40
  6:  20
  7:  10

format-by-age-0-table:
  Tablet:  1
  Scroll:  1
  Codex:   97

format-by-age-1-table:
  Tablet:  1
  Scroll:  2
  Codex:   96

format-by-age-2-table:
  Tablet:  1
  Scroll:  2
  Codex:   96

format-by-age-3-table:
  Tablet:  1
  Scroll:  4
  Codex:   95

format-by-age-4-table:
  Tablet:  1
  Scroll:  24
  Codex:   75

format-by-age-5-table:
  Tablet:  5
  Scroll:  75
  Codex:   20

format-by-age-6-table:
  Tablet:  25
  Scroll:  50
  Codex:   0

format-by-age-7-table:
  Tablet:  50
  Scroll:  50
  Codex:   0

material-by-format-Tablet-table:
  2:  15
  3:  4
  4:  1
  7:  60
  8:  20

material-by-format-Scroll-table:
  0:  30
  1:  20
  5:  30
  6:  20

material-by-format-Codex-table:
  0:  50
  1:  45
  2:  2
  3:  2
  4:  1

number-of-extant-copies-table:
  0:  10
  1:  15
  2:  25
  3:  15
  4:  10
  5:  10
  6:  5
  7:  5
  8:  5
