###############################################################################
# REQUIRED FIELDS #############################################################
hoard-count-required: 'number of hoards to roll; set on command line with hoard-count=X'
hoard-type-required: 'type of hoard to roll: A, B, C, D; set on command line with hoard-type=X'


###############################################################################
#INITIALIZERS #################################################################
hoard-init: |
  {cpk.{hoard-type}.selectmany[{hoard-count}].noshow}
  {spk.{hoard-type}.selectmany[{hoard-count}].noshow}
  {epk.{hoard-type}.selectmany[{hoard-count}].noshow}
  {gpk.{hoard-type}.selectmany[{hoard-count}].noshow}
  {ppk.{hoard-type}.selectmany[{hoard-count}].noshow}
  {gems.{hoard-type}.selectmany[{hoard-count}].noshow}
  {jewelry.{hoard-type}.selectmany[{hoard-count}].noshow}
  {magic-items.common.{hoard-type}.selectmany[{hoard-count}].noshow}
  {magic-items.uncommon.{hoard-type}.selectmany[{hoard-count}].noshow}
  {magic-items.rare.{hoard-type}.selectmany[{hoard-count}].noshow}
  {magic-items.very-rare.{hoard-type}.selectmany[{hoard-count}].noshow}
  {magic-items.legendary.{hoard-type}.selectmany[{hoard-count}].noshow}
  {cp-goods.selectmany[{hoard-thousands[cp]}].noshow}
  {sp-goods.selectmany[{hoard-thousands[sp]}].noshow}
  {ep-goods.selectmany[{hoard-thousands[ep]}].noshow}
  {gp-goods.selectmany[{hoard-thousands[gp]}].noshow}
  {pp-goods.selectmany[{hoard-thousands[pp]}].noshow}

###############################################################################
# ACCUMULATORS ################################################################
# accumulators aggregate all items previously selected from a list of tables.
# when evaluated, they use an extended formatting syntax:
#
#   {acc-name:outer-spec:left:sep:right:item-spec}
#
#   acc-name - the name of the accumulator
#   item-spec - formatting spec to apply to each item in the accumulator. this
#       will be specified like normal formatting specs, but uses parens instead
#       of curlies due to how python interprets nested curlies; parens in this
#       field will be converted to curlies before formatting. if actual curlies
#       should appear here, escape a paren with backslash, like \( or \). the
#       items from the accumulator will be provided as in str.format(*item),
#       where the first positional arg will be the item name, and the rest will
#       be counts.
#   sep - this string is inserted between each formatted item string. if the
#       separator should include a colon, escape it with backslash, like \:.
#   left, right - these strings bookend the formatted item strings. if either
#       will include a colon, escape it with a backslash.
#   outer-spec - final formatting to apply to the string resulting from all
#       other operations and is analogous to the part of a normal formatting
#       spec that follows a colon.
#
# handled escape codes:
#   \r - insert a carriage return
#   \n - insert a line feed
#   \t - insert a tab
#   ( or ) - parens are replaced with curlies
#   \( or \) - escaped parens are replaced with parens
#   \: - replaced with colon after fields are split
#   \\ - replaced with backslash
hoard-thousands-accumulator:
- cpk.{hoard-type}
- spk.{hoard-type}
- epk.{hoard-type}
- gpk.{hoard-type}
- ppk.{hoard-type}

hoard-gems-accumulator:
- gems.{hoard-type}
- jewelry.{hoard-type}

hoard-magic-items-accumulator:
- magic-items.common.{hoard-type}
- magic-items.uncommon.{hoard-type}
- magic-items.rare.{hoard-type}
- magic-items.very-rare.{hoard-type}
- magic-items.legendary.{hoard-type}

hoard-goods-accumulator:
- cp-goods
- sp-goods
- ep-goods
- gp-goods
- pp-goods


###############################################################################
# HOARD GOODS #################################################################
cp-goods-table:
- ['bags of grain or vegetables',         '{@[2d20]}']
- ['barrels of fish or pork',             '{@[1d3]}']
- ['tenth-coords of hardwood logs',       '{@[1d3]}']
- ['wheels of cheese',                    '{@[3d20]}']
- ['half-barrels of beer',                '{@[1d3]}']
- ['bricks of salt',                      '{@[4d6]}']
- ['ingots of common metals',             '{@[3d6]}']
- ['quarter-barrels of preserved meats',  '{@[1d3]}']
- ['rolls of roughspun cloth',            '1']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['cp',                                  '1000']
- ['sp',                                  '100']

sp-goods-table: [
  ['cp',                                                          '10000'],
  ['{livestock} (gp value)',                                      '{@[3d6*10]}'],
  ['jars of {oil-type} oil worth 20gp (6 st)',                    '{@[2d4]}'],
  ['captured laborers',                                           '{@[1d4]}'],
  ['10 yards of woven textiles worth 5gp (1 st)',                 '{@[1d3]}'],
  ['bundles of common fur pelts worth 15gp (1 st / 5gp)',         '{@[2d6]}'],
  ['captured domestic servant',                                   '1'],
  ['common animal horns worth {@[1d10]}gp each (1 st / 10gp)',    '{@[4d8]}'],
  ['jars of dyes and pigments worth 50gp (5 st)',                 '{@[1d3]}'],
  ['quarter-barrels of fine spirits or liquor worth 50gp (4 st)', '{@[1d3]}'],
  ['bags of loose tea or coffee worth 75 gp (5 st)',              '{@[1d2]}'],
  ['half-crates of terra-corra pottery worth 50 gp (2.5 st)',     '{@[1d3]}'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['sp',                                                          '1000'],
  ['gp',                                                          '100']    ]

livestock-table:
  - 'rabbit'
  - 'hen'
  - 'sheep'
  - 'sheep'
  - 'pig'
  - 'goat'
  - 'cattle'
  - 'horse'
  - 'horse'
  - 'horse'
  - 'yak'

oil-type-table: ['cooking', 'lamp']

ep-goods-table: [
  ['sp',                                                                        '5000'],
  ['captured {ep-prisoner-type} worth {@[1d4*100]} each',                       '{@[1d3]}'],
  ['crates of armor and weapons worth 225gp (10 st)',                           '{@[1d3]}'],
  ['bottles of fine wine worth 5gp (5 bottles / st)',                           '{@[2d100]}'],
  ['rugs of common fur pelts worth {@[2d4*5]}gp (1 st / 25gp)',                 '{@[3d12]}'],
  ['common bird feathers worth {@[1d3]}sp (1 st / 150)',                        '{@[2d4*500]}'],
  ['bundles of large common fur worth {@[1d8*15]} (1 st / 30gp)',               '{@[3d4]}'],
  ['uncommon animal horns worth {@[3d4*10]} (1 st / 40gp)',                     '{@[1d12]}'],
  ['crates of glassware worth 200gp (5 st)',                                    '{@[1d4]}'],
  ['bundles of large uncommon fur pelts worth {@[2d4*50]} each (1 st / 50 gp)', '{@[1d3]}'],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['ep',                                                                        1000],
  ['gp',                                                                        500]     ]

ep-prisoner-type-table: ['craftsman', 'merchant']

gp-goods-table: [
  ['sp',                                                         '10000'],
  ['ep',                                                         '2000'],
  ['metamphorae filled with souls (1 st / 60gp)',                '{@[4d4*100]}'],
  ['fresh monster carcasses with souls worth {@[1d10*50]}',      '{@[1d6]}'],
  ['monster feathers worth {@[1d6*(1d6)]}gp (1 st / 80gp)',      '{@[1d12*12]}'],
  ['monster horns worth {@[1d8*50]}gp (1 st / 80gp)',            '{@[1d8]}'],
  ['pieces of elephant ivory worth {@[1d100]} (1 st / 100 gp)',  '{@[2d20]}'],
  ['bundle of healing herbs worth 100gp (1 st)',                 '{@[4d4]}'],
  ['rolls of silk worth 400gp (4 st)',                           '{@[1d4]}'],
  ['bundle of rare fur pelt worth {@[2d4*100]} (1 st / 100 gp)', '{@[1d3]}'],
  ['captured {gp-prisoner-type} worth {@[2d4*200]}',             1],
  ['creates of fine porcelain worth 500gp (5 st)',               '{@[1d3]}'],
  ['rugs of large common fur worth {@[1d4*30]} (1 st / 150gp)',  '{@[4d6]}'],
  ['common books (gp value)',                                    '{@[4d4*250]}'],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['gp',                                                         1000],
  ['pp',                                                         100]     ]

gp-prisoner-type-table: ['equerry', 'lady-in-waiting', 'hetaera', 'odalisque']

pp-goods-table: [
  ['gp',                                                            '5000'],
  ['ingots of precious metals worth 300 gp (2 st)',                 '{@[3d10]}'],
  ['capes of common fur worth {@[1d6*50]} (1 st)',                  '{@[5d10]}'],
  ['rugs of large uncommon fur worth {@[1d4*250]} (1 st / 250gp)',  '{@[2d6+1]}'],
  ['rare books (gp value)',                                         '{@[3d10*300]}'],
  ['rare horns worth {@[1d4*150]} (1 st / 450gp)',                  '{@[2d12]}'],
  ['{pp-prisoner-type} worth {@[2d4*1000]}',                        1],
  ['coats of common fur worth {@[1d6*150]}gp (1 st)',               '{@[2d8]}'],
  ['jars of spices worth 800gp (1 st)',                             '{@[2d4+1]}'],
  ['unicorn or narwhale ivory worth @{[2d4*100]} (1 st / 1000 gp)', '{@[4d4]}'],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000],
  ['pp',                                                            1000]     ]

pp-prisoner-type-table: ['squire', 'damsel', 'gladiator', 'concubine']

###############################################################################
# HOARD TOTAL VALUES  #########################################################

# CPK #########################################################################
cpk:
  A-table:
  - weight: 70
    value: ['cp', 0]
  - weight: 30
    value: ['cp', '{@[2d4]}']
  B-table:
  - weight: 20
    value: ['cp', 0]
  - weight: 80
    value: ['cp', '{@[4d4]}']
  C-table:
  - weight: 65
    value: ['cp', 0]
  - weight: 35
    value: ['cp', '{@[2d4]}']
  D-table:
  - weight: 20
    value: ['cp', 0]
  - weight: 80
    value: ['cp', '{@[4d4]}']
  R-table:
  - weight: 20
    value: ['cp', 0]
  - weight: 80
    value: ['cp', '{@[7d6]}']

# SPK #########################################################################
spk:
  A-table:
  - weight: 70
    value: ['sp', 0]
  - weight: 30
    value: ['sp', '{@[1d3]}']
  B-table:
  - weight: 20
    value: ['sp', 0]
  - weight: 80
    value: ['sp', '{@[1d4]}']
  C-table:
  - weight: 65
    value: ['sp', 0]
  - weight: 35
    value: ['sp', '{@[1d3]}']
  D-table:
  - weight: 20
    value: ['sp', 0]
  - weight: 80
    value: ['sp', '{@[1d4]}']
  R-table:
  - weight: 20
    value: ['sp', 0]
  - weight: 80
    value: ['sp', '{@[5d6]}']

# EPK #########################################################################
epk:
  A-table:
  - weight: 100
    value: ['ep', 0]
  B-table:
  - weight: 100
    value: ['ep', 0]
  C-table:
  - weight: 90
    value: ['ep', 0]
  - weight: 10
    value: ['ep', '{@[1d3]}']
  D-table:
  - weight: 80
    value: ['ep', 0]
  - weight: 20
    value: ['ep', '{@[1d4]}']
  R-table:
  - weight: 20
    value: ['ep', 0]
  - weight: 80
    value: ['ep', '{@[2d6]}']

# GPK #########################################################################
gpk:
  A-table:
  - weight: 100
    value: ['gp', 0]
  B-table:
  - weight: 100
    value: ['gp', 0]
  C-table:
  - weight: 100
    value: ['gp', 0]
  D-table:
  - weight: 100
    value: ['gp', 0]
  R-table:
  - weight: 20
    value: ['gp', 0]
  - weight: 80
    value: ['gp', '{@[2d4]}']

# PPK #########################################################################
ppk:
  A-table:
  - weight: 100
    value: ['pp', 0]
  B-table:
  - weight: 100
    value: ['pp', 0]
  C-table:
  - weight: 100
    value: ['pp', 0]
  D-table:
  - weight: 100
    value: ['pp', 0]
  R-table:
  - weight: 25
    value: ['pp', 0]
  - weight: 75
    value: ['pp', '{@[1d6]}']

# GEMS ########################################################################
gems:
  A-table:
  - weight: 70
    value: ['ornamentals', 0]
  - weight: 30
    value: ['ornamentals', '{@[1d4]}']
  B-table:
  - weight: 30
    value: ['ornamentals', 0]
  - weight: 70
    value: ['ornamentals', '{@[1d4]}']
  C-table:
  - weight: 60
    value: ['gems', 0]
  - weight: 40
    value: ['gems', '{@[1d6]}']
  D-table:
  - weight: 20
    value: ['ornamentals', 0]
  - weight: 80
    value: ['ornamentals', '{@[1d6]}']
  R-table:
  - weight: 30
    value: ['brilliants', 0]
  - weight: 70
    value: ['brilliants', '{@[1d4]}']

# JEWELRY #####################################################################
jewelry:
  A-table:
  - weight: 70
    value: ['trinkets', 0]
  - weight: 30
    value: ['trinkets', '{@[1d4]}']
  B-table:
  - weight: 70
    value: ['trinkets', 0]
  - weight: 30
    value: ['trinkets', '{@[1d4]}']
  C-table:
  - weight: 70
    value: ['trinkets', 0]
  - weight: 30
    value: ['trinkets', '{@[1d6]}']
  D-table:
  - weight: 30
    value: ['trinkets', 0]
  - weight: 70
    value: ['trinkets', '{@[1d4]}']
  R-table:
  - weight: 40
    value: ['regalia', 0]
  - weight: 60
    value: ['regalia', '{@[1d4]}']

# MAGIC ITEMS #################################################################
magic-items:
  common:
    A-table:
    - weight: 80
      value: ['common', 0]
    - weight: 20
      value: ['common', 1]
    B-table:
    - weight: 50
      value: ['common', 0]
    - weight: 50
      value: ['common', 1]
    C-table:
    - weight: 75
      value: ['common', 0]
    - weight: 25
      value: ['common', 1]
    D-table:
    - weight: 50
      value: ['common', 0]
    - weight: 50
      value: ['common', '{@[2d2]}']
    R-table:
    - weight: 5
      value: ['common', 0]
    - weight: 95
      value: ['common', '{@[4d6]}']
  uncommon:
    A-table:
    - weight: 98
      value: ['uncommon', 0]
    - weight: 2
      value: ['uncommon', 1]
    B-table:
    - weight: 75
      value: ['uncommon', 0]
    - weight: 25
      value: ['uncommon', 1]
    C-table:
    - weight: 85
      value: ['uncommon', 0]
    - weight: 15
      value: ['uncommon', 1]
    D-table:
    - weight: 50
      value: ['uncommon', 0]
    - weight: 50
      value: ['uncommon', 1]
    R-table:
    - weight: 5
      value: ['uncommon', 0]
    - weight: 95
      value: ['uncommon', '{@[3d6]}']

  rare:
    A-table:
    - weight: 100
      value: ['rare', 0]
    B-table:
    - weight: 95
      value: ['rare', 0]
    - weight: 5
      value: ['rare', 1]
    C-table:
    - weight: 98
      value: ['rare', 0]
    - weight: 2
      value: ['rare', 1]
    D-table:
    - weight: 80
      value: ['rare', 0]
    - weight: 20
      value: ['rare', 1]
    R-table:
    - weight: 20
      value: ['rare', 0]
    - weight: 80
      value: ['rare', '{@[2d6]}']

  very-rare:
    A-table:
    - weight: 100
      value: ['very rare', 0]
    B-table:
    - weight: 100
      value: ['very rare', 0]
    C-table:
    - weight: 100
      value: ['very rare', 0]
    D-table:
    - weight: 100
      value: ['very rare', 0]
    R-table:
    - weight: 25
      value: ['very rare', 0]
    - weight: 75
      value: ['very rare', '{@[1d4]}']

  legendary:
    A-table:
    - weight: 100
      value: ['very rare', 0]
    B-table:
    - weight: 100
      value: ['very rare', 0]
    C-table:
    - weight: 100
      value: ['very rare', 0]
    D-table:
    - weight: 100
      value: ['very rare', 0]
    R-table:
    - weight: 50
      value: ['legendary', 0]
    - weight: 50
      value: ['legendary', '{@[1d2]}']
