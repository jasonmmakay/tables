import os

def system(cmd, *args, **kwargs):
    cmd = cmd.format(*args, **kwargs)
    return os.system(cmd)


if __name__ == '__main__':
    root = os.path.dirname(os.path.realpath(__file__))

    cmd = (
        'python {markov_py} train --order=4'
        ' {greek_feminine}={{w_greek_feminine}}'
        ' {greek_masculine}={{w_greek_masculine}}'
        ' {greek_surnames}={{w_greek_surnames}}'
        ' {japanese_feminine}={{w_japanese_feminine}}'
        ' {japanese_masculine}={{w_japanese_masculine}}'
        ' {japanese_neutral}={{w_japanese_neutral}}'
        ' {japanese_surnames}={{w_japanese_surnames}}'
        ' {{output}}')

    cmd = cmd.format(
        markov_py=os.path.join(root, '../../markov.py'),
        greek_feminine=os.path.join(root, '../names/greek/feminine.txt'),
        greek_masculine=os.path.join(root, '../names/greek/masculine.txt'),
        greek_surnames=os.path.join(root, '../names/greek/surnames.txt'),
        japanese_feminine=os.path.join(root, '../names/japanese/feminine.txt'),
        japanese_masculine=os.path.join(root, '../names/japanese/masculine.txt'),
        japanese_neutral=os.path.join(root, '../names/japanese/neutral.txt'),
        japanese_surnames=os.path.join(root, '../names/japanese/surnames.txt'))


    # weights are calculated so that each file contributes about the same
    # amount to the model, giving more weight to sequences in files with fewer
    # sequences and less to those in files with more seqeunces, so that each
    # file, representing a category, contributes the same weight to the model,
    # preventing categories with few sequences from being overwhelmed by those
    # with many sequences.
    #
    # ni, the number of sequences in file i
    # N = sum(ni), the total number of sequences
    # mi = N / ni, the inverse of the proportion of sequences in file i
    # M = sum(mi), the sum of proportion inverses
    # pi = mi / M, the normalized proportion inverse, weight per file
    # si = mi / ni, the weight per sequence in file i
    #
    # usually, all categories are used for each model, but they are weighted
    # further depending on which category should be emphasized (eg, give 100
    # times the weight to feminine names vs masculine names when building the
    # feminine name model).
    #
    # ai, the weight adjustment for the sequences in a file i
    # bi = si * ai, the adjusted weight for each sequence in file i
    # B = min(bi), the minimum adjusted weight for any sequence in any file
    # wi = round(bi / B), the integer weight for each sequence in file i, so
    #   that min(wi) == 1. NOTE: weights must be integers.
    #
    # TODO: the calculations were done via spreadsheet, but needs to be
    #   automated for the next update to training seqeunces

    # ai choices: feminine 100, masculine 5, neutral 50, surnames 1
    system(
        cmd,
        w_greek_feminine=63433,
        w_greek_masculine=2472,
        w_greek_surnames=46,
        w_japanese_feminine=4241,
        w_japanese_masculine=22,
        w_japanese_neutral=33470,
        w_japanese_surnames=1,
        output=os.path.join(root, 'feminine.markov'))

    # ai choices: feminine 5, masculine 100, neutral 50, surnames 1
    system(
        cmd,
        w_greek_feminine=3172,
        w_greek_masculine=49437,
        w_greek_surnames=46,
        w_japanese_feminine=212,
        w_japanese_masculine=448,
        w_japanese_neutral=33470,
        w_japanese_surnames=1,
        output=os.path.join(root, 'masculine.markov'))

    # ai choices: feminine 1, masculine 1, neutral 1, surnames 100
    system(
        cmd,
        w_greek_feminine=142,
        w_greek_masculine=110,
        w_greek_surnames=1035,
        w_japanese_feminine=9,
        w_japanese_masculine=1,
        w_japanese_neutral=150,
        w_japanese_surnames=22,
        output=os.path.join(root, 'surname.markov'))

    # the rest of these build a markov chain for each of the training files,
    # for analysis purposes
    system(
        cmd,
        w_greek_feminine=1,
        w_greek_masculine=0,
        w_greek_surnames=0,
        w_japanese_feminine=0,
        w_japanese_masculine=0,
        w_japanese_neutral=0,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/greek/feminine.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=1,
        w_greek_surnames=0,
        w_japanese_feminine=0,
        w_japanese_masculine=0,
        w_japanese_neutral=0,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/greek/masculine.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=0,
        w_greek_surnames=1,
        w_japanese_feminine=0,
        w_japanese_masculine=0,
        w_japanese_neutral=0,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/greek/surname.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=0,
        w_greek_surnames=0,
        w_japanese_feminine=1,
        w_japanese_masculine=0,
        w_japanese_neutral=0,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/japanese/feminine.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=0,
        w_greek_surnames=0,
        w_japanese_feminine=0,
        w_japanese_masculine=1,
        w_japanese_neutral=0,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/japanese/masculine.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=0,
        w_greek_surnames=0,
        w_japanese_feminine=0,
        w_japanese_masculine=0,
        w_japanese_neutral=1,
        w_japanese_surnames=0,
        output=os.path.join(root, '../names/japanese/neutral.markov'))

    system(
        cmd,
        w_greek_feminine=0,
        w_greek_masculine=0,
        w_greek_surnames=0,
        w_japanese_feminine=0,
        w_japanese_masculine=0,
        w_japanese_neutral=0,
        w_japanese_surnames=1,
        output=os.path.join(root, '../names/japanese/surname.markov'))
