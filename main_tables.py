# this should only ever be run from the virtual environment
import argparse
import random
import time

from src.debug import enable_debug, error, log, enable_log
from src.form import Form


def venv_main():
    """Virtual environment main."""
    parser = argparse.ArgumentParser(
        prog='tables.py',
        description='''
            Fill a form with randomly generated content and display the results
            on stdout. See README.md for full documentation.
            ''')

    parser.add_argument(
        'form', metavar='FORM',
        help='Form file to run')
    parser.add_argument(
        'properties', metavar='PROP=VALUE', nargs='*',
        help='''
            Additional form field definitions, in the form path.to.field=value;
            if the form contains any required fields, they must be specified
            via property
            ''')
    parser.add_argument(
        '--reset', action='store_true',
        help='Reset all state for this form first')
    parser.add_argument(
        '--seed', type=int, default=time.time_ns(),
        help='Random number generator seed')
    parser.add_argument(
        '--debug', action='store_true',
        help='Emit debug messages')
    parser.add_argument(
        '--log', metavar='MODULE',
        choices=(
            'all', 'field', 'form', 'formatting', 'macro', 'markov', 'state',
            'table'),
        default=[], nargs='+',
        help='Enable logging for specific modules: %(choices)s')

    args = parser.parse_args()

    enable_log('main')
    for module in args.log:
        enable_log(module)

    if args.debug:
        enable_debug()

    random.seed(args.seed)
    log('main', 'seed: {}', args.seed)

    form = Form(args.form, args.properties, args.reset)

    # make sure all requirements are fulfilled
    requirements = form.get_requirements()
    if requirements:
        requirements = (
            '{}: {}'.format(req._path, req._msg) for req in
            sorted(requirements, key=lambda req: req._path))

        msg = '\n  '.join((
            'the following requirements have not been fulfilled:',
            *requirements))

        error(msg)
        return

    print('{}'.format(form))


if __name__ == '__main__':
    venv_main()
