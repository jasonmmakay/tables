from src.dice import Dice
from src.formatting import recursive_format
from src.state import State
from src.struct import struct

from test.fixtures.data import data_basis


class TestState:

    def test_delayed_initialization(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 2])

        data = data_basis()
        data.test = State(
            'test',                     # name
            {'value': '{dice[d6+{self.value}]}',  # value
            'initial': '{dice[d6]}'},               # initial
            None,                       # current
            'test',                     # path
            data)                       # data

        assert not data.test._initialized
        assert not data.test._loading
        assert data.test._initial == '{dice[d6]}'
        assert recursive_format('{test.value}', **data) == ''

        assert recursive_format('{test}', **data) == '2'
        assert data.test._initialized
        assert not data.test._loading

        assert recursive_format('{test}', **data) == '8'

        assert recursive_format('{test.reset}', **data) == '3'
        assert data.test._initialized
        assert not data.test._loading
        assert recursive_format('{test.value}', **data) == '3'

    def test_loaded_value(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 2])

        data = data_basis()
        data.test = State(
            'test',                     # name
            {'value': '{dice[d6+{self.value}]}',  # value
            'initial': '{dice[d6]}'},               # initial
            '100',                      # current
            'test',                     # path
            data)                       # data

        assert not data.test._initialized
        assert data.test._loading
        assert data.test._initial == '{dice[d6]}'
        assert recursive_format('{test.value}', **data) == '100'

        assert recursive_format('{test}', **data) == '102'
        assert not data.test._loading
        assert data.test._initialized

        assert recursive_format('{test}', **data) == '108'

        assert recursive_format('{test.reset}', **data) == '3'
        assert data.test._initialized
        assert not data.test._loading
        assert recursive_format('{test.value}', **data) == '3'

    def test_value_no_current(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 2])

        data = data_basis()
        data.test = State(
            'test',                     # name
            {'value': '{dice[d6+{self.value}]}',  # value
            'initial': '{dice[d6]}' },               # initial
            None,                       # current
            'test',                     # path
            data)                       # data

        assert recursive_format('{test.value}', **data) == ''
        assert recursive_format('{test.value}', **data) == ''

    def test_value_with_current(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 2])

        data = data_basis()
        data.test = State(
            'test',                     # name
            {'value': '{dice[d6+{self.value}]}',  # value
            'initial': '{dice[d6]}'},               # initial
            '100',                      # current
            'test',                     # path
            data)                       # data

        assert recursive_format('{test.value}', **data) == '100'
        assert recursive_format('{test.value}', **data) == '100'

    def test_noshow(self):
        data = data_basis()
        data.test = State(
            'test',  # name
            {
                'value': '{@[1+{self.value}]}',
                'initial': '0'
            },
            None,   # current
            'test', # path
            data)   # data

        assert recursive_format('{test.noshow}', **data) == ''
        assert recursive_format('{test.value}', **data) == '0'  # initial

        assert recursive_format('{test.noshow}', **data) == ''
        assert recursive_format('{test.value}', **data) == '1'  # next
