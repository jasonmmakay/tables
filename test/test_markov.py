from src.markov import MarkovChain, Start, End


class TestMarkov:
    def test_single(self):
        m = MarkovChain([], 'test', 2)
        m.add(['abc'])

        assert len(m.chains) == 6
        assert m.chains[(Start, Start)] == {'a': 1}
        assert m.chains[(Start, 'a')] == {'b': 1}
        assert m.chains[('a', 'b')] == {'c': 1}
        assert m.chains[('b', 'c')] == {End: 1}
        assert m.chains[('c', End)] == {End: 1}
        assert m.chains[(End, End)] == {None: 1}

        assert len(m.back_chains) == 6
        assert m.back_chains[(End, None)] == {End: 1}
        assert m.back_chains[(End, End)] == {'c': 1}
        assert m.back_chains[('c', End)] == {'b': 1}
        assert m.back_chains[('b', 'c')] == {'a': 1}
        assert m.back_chains[('a', 'b')] == {Start: 1}
        assert m.back_chains[(Start, 'a')] == {Start: 1}

    def test_middle_divergence(self):
        m = MarkovChain([], 'test', 2)
        m.add(['abc', 'axc'])

        assert len(m.chains) == 8
        assert m.chains[(Start, Start)] == {'a': 2}
        assert m.chains[(Start, 'a')] == {'b': 1, 'x': 1}
        assert m.chains[('a', 'b')] == {'c': 1}
        assert m.chains[('a', 'x')] == {'c': 1}
        assert m.chains[('b', 'c')] == {End: 1}
        assert m.chains[('x', 'c')] == {End: 1}
        assert m.chains[('c', End)] == {End: 2}
        assert m.chains[(End, End)] == {None: 2}

        assert len(m.back_chains) == 8
        assert m.back_chains[(End, None)] == {End: 2}
        assert m.back_chains[(End, End)] == {'c': 2}
        assert m.back_chains[('c', End)] == {'b': 1, 'x': 1}
        assert m.back_chains[('b', 'c')] == {'a': 1}
        assert m.back_chains[('x', 'c')] == {'a': 1}
        assert m.back_chains[('a', 'b')] == {Start: 1}
        assert m.back_chains[('a', 'x')] == {Start: 1}
        assert m.back_chains[(Start, 'a')] == {Start: 2}

    def test_start_divergence(self):
        m = MarkovChain([], 'test', 2)
        m.add(['abc', 'xbc'])

        assert len(m.chains) == 8
        assert m.chains[(Start, Start)] == {'a': 1, 'x': 1}
        assert m.chains[(Start, 'a')] == {'b': 1}
        assert m.chains[(Start, 'x')] == {'b': 1}
        assert m.chains[('a', 'b')] == {'c': 1}
        assert m.chains[('x', 'b')] == {'c': 1}
        assert m.chains[('b', 'c')] == {End: 2}
        assert m.chains[('c', End)] == {End: 2}
        assert m.chains[(End, End)] == {None: 2}

        assert len(m.back_chains) == 8
        assert m.back_chains[(End, None)] == {End: 2}
        assert m.back_chains[(End, End)] == {'c': 2}
        assert m.back_chains[('c', End)] == {'b': 2}
        assert m.back_chains[('b', 'c')] == {'a': 1, 'x': 1}
        assert m.back_chains[('a', 'b')] == {Start: 1}
        assert m.back_chains[('x', 'b')] == {Start: 1}
        assert m.back_chains[(Start, 'a')] == {Start: 1}
        assert m.back_chains[(Start, 'x')] == {Start: 1}

    def test_end_divergence(self):
        m = MarkovChain([], 'test', 2)
        m.add(['abc', 'abx'])

        assert len(m.chains) == 8
        assert m.chains[(Start, Start)] == {'a': 2}
        assert m.chains[(Start, 'a')] == {'b': 2}
        assert m.chains[('a', 'b')] == {'c': 1, 'x': 1}
        assert m.chains[('b', 'c')] == {End: 1}
        assert m.chains[('b', 'x')] == {End: 1}
        assert m.chains[('c', End)] == {End: 1}
        assert m.chains[('x', End)] == {End: 1}
        assert m.chains[(End, End)] == {None: 2}

        assert len(m.back_chains) == 8
        assert m.back_chains[(End, None)] == {End: 2}
        assert m.back_chains[(End, End)] == {'c': 1, 'x': 1}
        assert m.back_chains[('c', End)] == {'b': 1}
        assert m.back_chains[('x', End)] == {'b': 1}
        assert m.back_chains[('b', 'c')] == {'a': 1}
        assert m.back_chains[('b', 'x')] == {'a': 1}
        assert m.back_chains[('a', 'b')] == {Start: 2}
        assert m.back_chains[(Start, 'a')] == {Start: 2}
