from src.struct import struct
from src.dice import Dice
from src.self import SelfStack


def data_basis():
    """
    Construct a basic data object for unit tests.

    The data object includes:
        - dice objects (both dice and @)
        - self object
    """
    data = struct()
    data.dice = dice=Dice(deprecated=True)
    data['@'] = Dice()
    data.self = SelfStack()
    return data