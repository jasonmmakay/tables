import pytest

from src.dice import Dice
from src.struct import struct
from src.formatting import recursive_format
from src.dice import infix_to_postfix
from src.dice import split_atoms
from src.dice import evaluate_postfix


class TestDice:
    def test_dice(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4])

        data = struct(dice=Dice())
        assert '{dice[4d6]}'.format(**data) == '12'

    def test_arithmetic(self):
        data = struct(dice=Dice())
        assert '{dice[-5+2]}'.format(**data) == '-3'

    def test_dice_and_arithmetic(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[7, 9, 10, 9, 7, 10, 7, 2, 8, 7])

        data = struct(dice=Dice())
        assert '{dice[-100+10d10+10]}'.format(**data) == '-14'

    def test_default_one_die(self, mocker):
        mocker.patch('src.dice.random.randint', side_effect=[7, 9, 2, 3])
        data = struct(dice=Dice())
        assert '{dice[d10]}'.format(**data) == '7'
        assert '{dice[3+d10]}'.format(**data) == '12'
        assert '{dice[10-d10]}'.format(**data) == '8'
        assert '{dice[d10+5]}'.format(**data) == '8'

    def test_keep(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[3, 4, 1, 5, 3, 4, 1, 5, 3, 4, 1, 5])
        data = struct(dice=Dice())

        assert '{dice[4d6k3]}'.format(**data) == '12'
        assert '{dice[4d6k-3]}'.format(**data) == '8'
        assert '{dice[4d6k3k-2]}'.format(**data) == '7'

    def test_nested_format(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4])

        data = struct(dice=Dice(), n=2)
        # NOTE: this expression requires recusrive formatting
        assert recursive_format('{dice[{n}d6]}', **data) == '8'

    def test_if(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[20, 2, 80, 1])

        data = struct(dice=Dice())
        assert '{dice[if(1d100>50,d2,0)]}'.format(**data) == '0'
        assert '{dice[if(1d100>50,d2,0)]}'.format(**data) == '1'

    def test_explode(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[
                2, 6, 3,
                2, 6, 3, 5,
                5, 6, 3, 5, 1,
                1, 5, 3, 6,
                2, 6,
                2, 6, 1, 6, 1, 3])

        data = struct(dice=Dice())

        assert '{dice[2d6!6]}'.format(**data) == '11'

        # TODO: this is very inefficient, we should look for both values at the
        # same time
        assert '{dice[2d6!6!3]}'.format(**data) == '16'
        assert '{dice[2d6!>4]}'.format(**data) == '20'
        assert '{dice[2d6!<4]}'.format(**data) == '15'
        assert '{dice[2d6!>0]}'.format(**data) == 'inf'

        assert '{dice[2d6!1!6]}'.format(**data) == '19'

    def test_penetrating_explode(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[
                2, 6, 3,
                2, 6, 3, 5,
                5, 6, 3, 5, 1,
                1, 5, 3, 6,
                2, 6,
                2, 6, 1, 6, 1, 3])

        data = struct(dice=Dice())

        assert '{dice[2d6!p6]}'.format(**data) == '10'

        # TODO: this is very inefficient, we should look for both values at the
        # same time
        assert '{dice[2d6!p6!p3]}'.format(**data) == '14'
        assert '{dice[2d6!p>4]}'.format(**data) == '17'
        assert '{dice[2d6!p<4]}'.format(**data) == '13'
        assert '{dice[2d6!>0]}'.format(**data) == 'inf'

        assert '{dice[2d6!p1!p6]}'.format(**data) == '15'


def expr_to_postfix(expr):
    return infix_to_postfix(split_atoms(expr))


def evaluate_expr(expr):
    return evaluate_postfix(expr_to_postfix(expr))


class TestExpressions:
    def test_split_atoms(self):
        assert split_atoms('-5') == ['u-', 5]
        assert split_atoms('--5') == ['u-', 'u-', 5]
        assert split_atoms('1-5') == [1, '-', 5]
        assert split_atoms('1+-5') == [1, '+', 'u-', 5]
        assert split_atoms('1--5') == [1, '-', 'u-', 5]

        assert split_atoms('2d6') == [2, 'd', 6]
        assert split_atoms('-2d6') == ['u-', 2, 'd', 6]
        assert split_atoms('d6') == ['ud', 6]
        assert split_atoms('-d6') == ['u-', 'ud', 6]
        assert split_atoms('2-d6') == [2, '-', 'ud', 6]
        assert split_atoms('2+-d6') == [2, '+', 'u-', 'ud', 6]
        assert split_atoms('2*d6') == [2, '*', 'ud', 6]
        assert split_atoms('2d4d6') == [2, 'd', 4, 'd', 6]
        assert split_atoms('-2d-6') == ['u-', 2, 'd', 'u-', 6]
        assert split_atoms('-2-d6') == ['u-', 2, '-', 'ud', 6]
        assert split_atoms('-2-d-6') == ['u-', 2, '-', 'ud', 'u-', 6]
        assert split_atoms('2dd6') == [2, 'd', 'ud', 6]

    def test_infix_to_postix(self):
        assert expr_to_postfix('-5') == [5, 'u-']
        assert expr_to_postfix('--5') == [5, 'u-', 'u-']
        assert expr_to_postfix('1-5') == [1, 5, '-']
        assert expr_to_postfix('1+-5') == [1, 5, 'u-', '+']
        assert expr_to_postfix('1--5') == [1, 5, 'u-', '-']

        assert expr_to_postfix('2d6') == [2, 6, 'd']
        assert expr_to_postfix('-2d6') == [2, 'u-', 6, 'd']
        assert expr_to_postfix('d6') == [6, 'ud']
        assert expr_to_postfix('-d6') == [6, 'ud', 'u-']
        assert expr_to_postfix('2-d6') == [2, 6, 'ud', '-']
        assert expr_to_postfix('2+-d6') == [2, 6, 'ud', 'u-', '+']
        assert expr_to_postfix('2*d6') == [2, 6, 'ud', '*']
        assert expr_to_postfix('2d4d6') == [2, 4, 6, 'd', 'd']
        assert expr_to_postfix('-2d-6') == [2, 'u-', 6, 'u-', 'd']
        assert expr_to_postfix('-2-d6') == [2, 'u-', 6, 'ud', '-']
        assert expr_to_postfix('-2-d-6') == [2, 'u-', 6, 'u-', 'ud', '-']
        assert expr_to_postfix('2dd6') == [2, 6, 'ud', 'd']

    def test_evaluate_postix(self, mocker):
        assert evaluate_expr('-5') == -5
        assert evaluate_expr('1-5') == -4
        assert evaluate_expr('1+-5') == -4
        assert evaluate_expr('1--5') == 6
        assert evaluate_expr('2**3') == 8
        assert evaluate_expr('7//3') == 2

        mocker.patch(
            'src.dice.random.randint',
            side_effect=[
                2, 5,
                2, 5,
                2,
                5,
                2,
                5,
                2,
                1, 3, 5, 2, 3, 1,
                2, 5,
                2,
                5,
                3, 1, 2])

        assert evaluate_expr('2d6') == 7        # 2, 5
        assert evaluate_expr('-2d6') == -7      # 2, 5
        assert evaluate_expr('d6') == 2         # 2
        assert evaluate_expr('-d6') == -5       # 5
        assert evaluate_expr('2-d6') == 0       # 2
        assert evaluate_expr('2+-d6') == -3     # 5
        assert evaluate_expr('2*d6') == 4       # 2
        assert evaluate_expr('2d4d6') == 4      # 1, 3, 5, 2, 3, 1
        assert evaluate_expr('-2d-6') == 7      # 2, 5
        assert evaluate_expr('-2-d6') == -4     # 2
        assert evaluate_expr('-2-d-6') == 3     # 5
        assert evaluate_expr('2dd6') == 3       # 3, 1, 2

    def test_logical_operators(self):
        assert evaluate_expr('-1<2')
        assert evaluate_expr('-1<=2')
        assert not evaluate_expr('-1>2')
        assert not evaluate_expr('-1>=2')
        assert not evaluate_expr('-1==2')
        assert evaluate_expr('-1!=2')
        assert evaluate_expr('1<2&5<7')
        assert not evaluate_expr('1<2&5>7')
        assert not evaluate_expr('1<2^5<7')
        assert evaluate_expr('1<2^5>7')
        assert evaluate_expr('1<2|5>7')
        assert not evaluate_expr('1>2|5>7')
        assert evaluate_expr('~(1>2)')

    def test_if(self):
        assert (
            split_atoms('if(5<7,1,2)')
            == ['if', '(', 5, '<', 7, ',', 1, ',', 2, ')'])

        assert (
            split_atoms('if(1,-1,2)')
            == ['if', '(', 1, ',', 'u-', 1, ',', 2, ')'])

        assert (
            expr_to_postfix('if(5<7,1,2)')
            == [5, 7, '<', 1,  2, 'if'])

        assert evaluate_expr('if(5<7,1,2)') == 1
        assert evaluate_expr('if(5>7,1,2)') == 2

    def test_float(self):
        assert evaluate_expr('2.3') == pytest.approx(2.3)
        assert evaluate_expr('2.3+4.1') == pytest.approx(6.4)
        assert evaluate_expr('floor(2.3)') == 2
        assert evaluate_expr('ceil(2.3)') == 3

        with pytest.raises(Exception) as ex:
            evaluate_expr('2.3d10')
        assert ex.value.args[0] == 'Die roll operands must be ints or die rolls'

        with pytest.raises(Exception) as ex:
            evaluate_expr('2d3.5')
        assert ex.value.args[0] == 'Die roll operands must be ints or die rolls'

        with pytest.raises(Exception) as ex:
            evaluate_expr('2d10!3.1')
        assert ex.value.args[0] == 'Explosion operands must be ints or die rolls'

        with pytest.raises(Exception) as ex:
            evaluate_expr('5d10k1.3')
        assert ex.value.args[0] == 'Right keep operand must be an int'
