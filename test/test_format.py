from src.formatting import recursive_format, iter_format

from test.fixtures.data import data_basis


class TestMacro:
    def setup_function(function):
        disable_log()

    def teardown_function(function):
        pass

    def test_basis(self):
        class DummyFormatter:
            def __init__(self):
                self.spec = None

            def __format__(self, spec):
                self.spec = spec
                return ''

        df = DummyFormatter()

        '{df:OUTER:LEFT:SEP:RIGHT:UNPACK:INNER}'.format(df=df)
        assert df.spec == 'OUTER:LEFT:SEP:RIGHT:UNPACK:INNER'

    def test_iter_format_no_expansion(self):
        obj = [[1, 2], [4, 8]]

        assert (
            iter_format('^15:<:~:>::(0[1])+(0[0])', obj, obj, '')
            == '   <2+1~8+4>   ')

        obj = [
            {'a': 1, 'b': 2},
            {'a': 4, 'b': 8}]

        assert (
            iter_format('^15:<:~:>::(0[b])+(0[a])', obj, obj, '')
            == '   <2+1~8+4>   ')

    def test_iter_format_star(self):
        obj = [[1, 2], [4, 8]]

        assert (
            iter_format('^15:<:~:>:*:(1)+(0)', obj, obj, '')
            == '   <2+1~8+4>   ')

    def test_iter_format_star_star(self):
        obj = [
            {'a': 1, 'b': 2},
            {'a': 4, 'b': 8}]

        assert (
            iter_format('^15:<:~:>:**:(b)+(a)', obj, obj, '')
            == '   <2+1~8+4>   ')



