import subprocess


def system(*cmd):
    result = subprocess.run(cmd, capture_output=True)
    return result.stdout.decode(), result.stderr.decode()


def load(file):
    with open(file, 'r') as f:
        return f.read()



class TestData:
    def test_acks_market(self):
        expected = load('test/fixtures/acks-market-no-props.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/market.form',)
        assert not stdout
        assert expected == stderr

        expected = load('test/fixtures/acks-market.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/market.form',
            'market.class=3',
            'market.name=Cyfaraun')
        assert expected == stdout
        assert not stderr

    def test_acks_henches(self):
        expected = load('test/fixtures/acks-henches-no-props.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/henches.form',)
        assert not stdout
        assert expected == stderr

        expected = load('test/fixtures/acks-henches.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/henches.form',
            'market.class=3',
            'market.name=Cyfaraun')
        assert expected == stdout
        assert not stderr

    def test_acks_books(self):
        expected = load('test/fixtures/acks-books.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/books.form',)
        assert expected == stdout
        assert not stderr

    def test_acks_books(self):
        expected = load('test/fixtures/acks-treasure-hoard.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            'data/acks/treasure-hoard.form',
            'hoard-count=5',
            'hoard-type=D')
        assert expected == stdout
        assert not stderr

    def test_hexflower(self):
        expected = load('test/fixtures/hexflower.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            '--reset',
            'test/fixtures/hexflower.form')
        assert expected == stdout
        assert not stderr

    def test_telestia_character(self):
        expected = load('test/fixtures/telestia-character.txt')
        stdout, stderr = system(
            'python',
            'tables.py',
            '--seed=0',
            '--reset',
            'data/telestia/character.form')
        assert expected == stdout
        assert not stderr
