from src.debug import enable_log, disable_log, log
from src.dice import Dice
from src.struct import struct
from src.field import Field
from src.macro import Macro
from src.state import State
from src.formatting import recursive_format

from test.fixtures.data import data_basis


class TestMacro:
    def setup_function(function):
        disable_log()

    def teardown_function(function):
        pass

    def test_macro(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4])

        data = data_basis()
        data.macro = Macro('macro', '{dice[2d6]}', 'macro', data)

        assert recursive_format('{macro}', **data) == '8'
        assert recursive_format('{macro}', **data) == '4'

    def test_macro_nested_in_field(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 5])

        data = data_basis()
        data.macro = Macro('macro', '{dice[2d6]}', 'macro', data)
        data.field = Field('field', '{macro}', 'field', data)

        assert recursive_format('{field}', **data) == '8'
        assert recursive_format('{macro}', **data) == '4'
        assert recursive_format('{macro}', **data) == '9'
        assert recursive_format('{field}', **data) == '8'

    def test_macro_nested_in_state(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 5])

        data = data_basis()
        data.macro = Macro('macro', '{dice[2d6]}', 'macro', data)
        data.state = State('state', {'value': '{macro}', 'initial': '{macro}'}, None, 'state', data)

        assert recursive_format('{state}', **data) == '8'
        assert recursive_format('{state.value}', **data) == '8'
        assert recursive_format('{state}', **data) == '4'
        assert recursive_format('{state.value}', **data) == '4'

    def test_many(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 5])

        data = data_basis()
        data.macro = Macro('macro', '{dice[2d6]}', 'macro', data)

        assert recursive_format('{macro.many[2]:::|:::(0)}', **data) == '8|4'

    def test_value(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4, 5])

        data = data_basis()
        data.macro = Macro('macro', '{dice[2d6]}', 'macro', data)

        assert recursive_format('{macro.value}', **data) == ''
        assert recursive_format('{macro}', **data) == '8'
        assert recursive_format('{macro.value}', **data) == '8'
        assert recursive_format('{macro}', **data) == '4'
        assert recursive_format('{macro.value}', **data) == '4'

    def test_other_self(self):
        data = data_basis()
        data.macro1 = Macro('macro1', '1 {self.name} 1', 'unit.macro1', data)
        data.macro2 = Macro('macro2', '2 {macro1} 2', 'unit.macro2', data)
        data.ustate = State('ustate', {'value': 'x {macro2} x', 'initial': 's {macro2} s'}, None, 'unit.state', data)

        assert recursive_format('f {ustate} f', **data) == 'f s 2 1 ustate 1 2 s f'


