from src.debug import enable_log, disable_log, log
from src.dice import Dice
from src.struct import struct
from src.field import Field
from src.formatting import recursive_format

from test.fixtures.data import data_basis


class TestField:
    def setup_function(function):
        disable_log()

    def teardown_function(function):
        pass

    def test_string(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[2, 6, 3, 1, 4])

        data = data_basis()
        data.field = Field('test', '{dice[2d6]}', 'test', data)

        assert recursive_format('{field}', **data) == '8'
        assert recursive_format('{field}', **data) == '8'

    def test_nonformat_string(self):
        data = data_basis()
        data.field = Field('test', '7', 'test', data)
        assert recursive_format('{field}', **data) == '7'
        assert recursive_format('{field}', **data) == '7'

    def test_number(self):
        data = data_basis()
        data.field = Field('test', 3, 'test', data)
        assert recursive_format('{field}', **data) == '3'
        assert recursive_format('{field}', **data) == '3'

    def test_nested_strings(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[
                2, 6,
                3, 1, 4, 1, 2, 2, 3, 4,
                1])

        data = data_basis()
        data.field1 = Field('test1', '{dice[2d6]}', 'test[1]', data)
        data.field2 = Field('test2', '{dice[{field1}d4]}', 'test[2]', data)

        assert recursive_format('{field2}', **data) == '20'
        assert recursive_format('{field2}', **data) == '20'
        assert recursive_format('{field1}', **data) == '8'
        assert recursive_format('{field1}', **data) == '8'
