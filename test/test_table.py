from src.form import objects_from_dict
from src.table import Table
from src.dice import Dice

import pytest
import yaml


class TestTables:

    def test_construction(self):
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})
        assert data.simple[0] == 'A'
        assert data.simple[1] == 'B'
        assert data.simple[2] == 'C'
        assert len(data.simple._items) == 3
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is None

    def test_select(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 2, 0, 1])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        actual = '{simple}{simple}{simple}{simple}{simple}'.format(**data)
        assert actual == 'BACAB'

        assert len(data.simple._items) == 3
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is not None

    def test_select_explicit(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 2, 0, 1])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        actual = (
            '{simple.select}{simple.select}{simple.select}'
            '{simple.select}{simple.select}'.format(**data))
        assert actual == 'BACAB'

        assert len(data.simple._items) == 3
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is not None

    def test_take(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 0])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        actual = '{simple.take}{simple.take}{simple.take}'.format(**data)
        assert actual == 'BAC'

        assert len(data.simple._items) == 0
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is not None

        with pytest.raises(Exception) as ex:
            '{simple.take}'.format(**data)
        assert ex.value.args[0] == 'Table is empty'

    def test_value(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 2, 0, 1])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        assert '{simple.value}'.format(**data) == ''

        assert '{simple}'.format(**data) == 'B'
        assert '{simple.value}'.format(**data) == 'B'

        assert '{simple.select}'.format(**data) == 'A'
        assert '{simple.value}'.format(**data) == 'A'

        assert '{simple.take}'.format(**data) == 'C'
        assert '{simple.value}'.format(**data) == 'C'

        assert len(data.simple._items) == 2
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is not None

    def test_reset(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 2, 0, 1])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        assert '{simple}{simple.value}{simple.take}'.format(**data) == 'BBA'
        assert len(data.simple._items) == 2
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is not None

        assert '{simple.reset}'.format(**data) == ''
        assert len(data.simple._items) == 3
        assert len(data.simple._original_items) == 3
        assert data.simple._selected is None
        assert '{simple.value}'.format(**data) == ''

    def test_selected(self, mocker):
        mocker.patch(
            'src.table.random.uniform',
            side_effect=[1, 0, 2, 0, 1] + [4, 2, 0, 3, 1])
        data = objects_from_dict(yaml.load('simple-table: ["A", "B", "C"]', Loader=yaml.BaseLoader), {}, {})

        actual = '{simple}{simple}{simple}{simple}{simple}'.format(**data)
        assert actual == 'BACAB'
        assert len(data.simple._selected._items) == 5

        actual = (
            '{simple.selected}{simple.selected}{simple.selected}'
            '{simple.selected}{simple.selected}'.format(**data))
        assert actual == 'BCBAA'

    def test_condition(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[0, 0, 0])

        data = yaml.load(
            '''
            test-table:
            - condition: 'False'
              value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        assert '{test.take}{test.take}'.format(**data) == 'BC'

        with pytest.raises(Exception) as ex:
            '{test.take}'.format(**data)
        assert ex.value.args[0] == 'Conditions have inactivated all items'

    def test_select_multi_valued(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[0, 0, 0])
        data = yaml.load(
            '''
            test-table:
            - value: ['A', 'B', 'C']
            - value: ['D', 'E', 'F']
            - value: ['G', 'H', 'I']
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        assert '{test.select[1]}'.format(**data) == 'B'
        assert '{test.value[0]}'.format(**data) == 'A'
        assert '{test.value[1]}'.format(**data) == 'B'
        assert '{test.value[2]}'.format(**data) == 'C'
        assert '{test.value}'.format(**data) == "['A', 'B', 'C']"

        # interpolation parses -1 as a string, not integer, raising TypeError
        # assert '{test.value[-1]}'.format(**data) == 'C'

    def test_select_many(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[2, 0, 1])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        assert '{test.selectmany[2]}'.format(**data) == 'C, A'

    def test_select_many_not_int(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[2, 0, 1])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        with pytest.raises(Exception) as ex:
            '{test.selectmany[bad]}'.format(**data)
        assert ex.value.args[0] == 'test.selectmany count must be an int'

    def test_select_many_double_index(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[2, 0, 1])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        with pytest.raises(Exception) as ex:
            '{test.selectmany[1][1]}'.format(**data)
        assert ex.value.args[0] == 'test.selectmany already selected'

    def test_select_many_format(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[2, 0, 1])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        assert (
            r'{test.selectmany[2]::<:~:>::-()+}'.format(**data)
            == '<-C+~-A+>')

    def test_select_many_bare(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[0, 0, 0])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        with pytest.raises(Exception) as ex:
            '{test.selectmany}'.format(**data)
        assert ex.value.args[0] == 'test.selectmany must provide a count'

    def test_take_too_many(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[0, 0, 0])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        with pytest.raises(Exception) as ex:
            '{test.takemany[4]}'.format(**data)
        assert ex.value.args[0] == 'Table is empty'

    def test_no_show(self, mocker):
        mocker.patch('src.table.random.uniform', side_effect=[1, 0, 0])

        data = yaml.load(
            '''
            test-table:
            - value: 'A'
            - value: 'B'
            - value: 'C'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        assert '{test.noshow}'.format(**data) == ''
        assert '{test.value}'.format(**data) == 'B'

    def test_weight(self):
        data = yaml.load(
            '''
            test-table:
            - value: 'A'
              weight: 2
            - value: 'B'
              count: 3
            - value: 'C'
              weight: 5
              count: 7
            - value: 'D'
              weight: 11
              count: 13
              no-count-weight: 0
            - value: 'E'
              weight: 17
              count: 19
              no-count-weight: 1
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        # weights and counts are parsed from strings on the first formatting
        # of the table (in case they are format strings themselves and need
        # dependencies loaded)
        '{test}'.format(**data)

        data.test._items[0].weight == 2
        data.test._items[1].weight == 3
        data.test._items[2].weight == 35
        data.test._items[3].weight == 143
        data.test._items[4].weight == 17

    def test_weight_expression(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[91, 79])

        data = yaml.load(
            '''
            test-table:
            - value: 'X'
              weight: '{@[d100]}'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        data['@'] = Dice()

        '{test}'.format(**data)
        assert data.test._items[0].weight == 91
        '{test}'.format(**data)
        assert data.test._items[0].weight == 91

        '{test.reset}'.format(**data)
        '{test}'.format(**data)
        assert data.test._items[0].weight == 79
        '{test}'.format(**data)
        assert data.test._items[0].weight == 79

    def test_count_expression(self, mocker):
        mocker.patch(
            'src.dice.random.randint',
            side_effect=[91, 79])

        data = yaml.load(
            '''
            test-table:
            - value: 'X'
              count: '{@[d100]}'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})
        data['@'] = Dice()

        '{test}'.format(**data)
        assert data.test._items[0].count == 91
        '{test}'.format(**data)
        assert data.test._items[0].count == 91

        '{test.reset}'.format(**data)
        '{test}'.format(**data)
        assert data.test._items[0].count == 79
        '{test}'.format(**data)
        assert data.test._items[0].count == 79

    def test_count_rounding(self):
        data = yaml.load(
            '''
            test-table:
            - value: 'X'
              count: 3.1
            - value: 'Y'
              count: '9.7'
            ''',
            Loader=yaml.BaseLoader)

        data = objects_from_dict(data, {}, {})

        # rounding happens in TableItem.initialize, which is triggered on the
        # first selection from the table
        '{test}'.format(**data)

        assert data.test._items[0].count == 3
        assert data.test._items[1].count == 10
