# tables.py
This is a tool designed with randomly generating formatted text content for
tabletop role playing games in mind, from treasure hoards to character sheets.
It leverages
[python's string formatting syntax](https://docs.python.org/3/library/string.html#format-string-syntax)
to allow for forms to be populated from a variety of random generation sources
including tables and markov chains for names.


[TOC]


# Installation
You will need the following to run this application:

- [Python 3.8.2](https://www.python.org/downloads/release/python-382/). While
there shouldn't be much trouble with running on other versions of python3, none
are tested or guaranteed to work.
- You will need pip (to install pipenv), which should be part of the default
python installation.
- Install pipenv by running the following command at an elevated command prompt:
`pip install pipenv`
- Python package dependencies: pyyaml, toml (supported, but yaml is preferred),
numpy.  All packages can be installed by running the following command from the
project directory: `pipenv install`


## Linux
Your distribution may not come with python3.8.2, and it may need to be installed
from source, [follow these instructions](https://mattgosden.medium.com/upgrade-to-python-3-8-on-a-chromebook-3dd81d370973).

Whether the correct version is installed, python and pip may be installed as
python3 and pip3, and you may wish to create aliases for them in ~/.profile
```
# alias python and pip
if [ -e "/usr/bin/python3" ] ; then
    alias python=python3
fi

if [ -e "/usr/bin/pip3" ] ; then
    alias pip=pip3
fi
```

Your installation of pipenv may need to be added to the $PATH in ~/.profile,
after it is installed
```
# add pipenv and other python scripts to the path
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
```


# TL;DR - Full Example
If you want to figure it out yourself without bothering with the rest of this,
here's a full working example to generate a Fate Core character with names from
my homebrew campaign setting and enneagram personality traits.

You'll need to know that:
- forms are the text files that define what the output looks like
- using [python's string formatting syntax](https://docs.python.org/3/library/string.html#format-string-syntax)
- and links to [yaml files](https://learnxinyminutes.com/docs/yaml/) that define
tables, markov chains and other structured data

Here are links to the files used in this example:
- [character.form](/data/telestia/character.form)
- [names.yaml](/data/telestia/names.yaml)
- [fate.yaml](/data/fate.yaml)
- [generic.yaml](/data/generic.yaml)
- [enneagram.yaml](/data/enneagram.yaml)
- [telestia.yaml](/data/telestia.yaml)

And the command line invocation, from within the project directory, and the
output:

```
> python tables.py data/telestia/character.form

Name:   Ura
Gender: female

Skills:
   Superb (+5)
    Great (+4)  Ride
     Good (+3)  Notice       Rapport
     Fair (+2)  Contacts     Shoot        Will
  Average (+1)  Provoke      Deceive      Empathy      Investigate

Stunts:
  Provoke Violence (Provoke)
  Demagogue (Rapport)
  Hard Boiled (Will)

Personality:
  Base:      (2) Helper, Giver
  Stressed:  (8) Challenger, Protector
  Relaxed:   (4) Individualist, Romantic
```


# Running the Application
- Open a command line in the project directory
- Run the following command: `python tables.py [path to form]`
- Output is printed to stdout


## Usage
```
usage: tables.py [-h] [--reset] [--seed SEED] [--debug]  [--log MODULE [MODULE ...]] FORM  [PROP=VALUE [PROP=VALUE ...]]

Fill a form with randomly generated content and display the results on stdout. See README.md for full documentation.

positional arguments:
  FORM                  Form file to run
  PROP=VALUE            Additional form field definitions, in the form path.to.field=value; if the form contains any required fields, they must be specified via property

optional arguments:
  -h, --help            show this help message and exit
  --reset               Reset all state for this form first
  --seed SEED           Random number generator seed
  --debug               Emit debug messages
  --log MODULE [MODULE ...]
                        Enable logging for specific modules: all, field, form, formatting, macro, markov, state, table
```


## Virtual Environments
This application is designed to be run in a virtual environment to prevent it
from polluting other python application modules, and aside from installation,
the user need not ever bother with the virtual environment again - tables.py
will automatically start it if necessary.

However, this comes with some cost at run time as the virtual environment must
be spun up each time tables.py is run. To avoid this overhead, a virtual
machine can be started manually and the application will detect that it is
already running in it. Run the command `pipenv shell` and then run tables.py
as usual. To exit the pipenv shell session, run `exit`.


# Developer Installation and Usage
Developers should run these commands in addition to the normal installation
steps.
```
git clone https://gitlab.com/jasonmmakay/tables.git
cd tables
git checkout --track origin/master
pipenv install --dev
```


## Running the Unit Tests
**Requires that the developer installation process has been completed**
The pytest module is installed as part of the virtual environment, but unlike
the main application, the shell must be manually started to run the tests.
```
pipenv shell
python -m pytest
```

## Running flake8
The linter must also be run from within a pipenv shell.

**NOTE: unit tests are not currently checked by the linter**

```
pipenv shell
flake8 src *.py
```

## Rendering README.md to HTML
```
pipenv shell
mdv -H README.md > README.html
```


# Forms
Forms are nothing more than text files that may contain python's string
formatting notation and statements that link the data files that contain
definitions for tables, markov chains and other structured data.

Lines that begin with `#include` will load the file named immediately after,
no quotation marks are necessary, and the file path is assumed to be relative
to the form file. Nothing is emitted to the results for these lines.

Any string format notation is replaced with values from the data files. The
only deviation from python's standard string formatting is support for nested
expressions, where the result of an inner expression can be used to finish a
name appearing in the enclosing expression.

Otherwise, this file is very much WYSIWYG, by design.


# Data Structures
[Defining Data Structures](#defining-data-structures)
[Tables](#tables)
[Accumulators](#accumulators)
[Fields](#fields)
[Macros](#macros)
[Requirements](#requirements)
[States](#states)
[Markov Chains](#markov-chains)


## Defining Data Structures
All data structures used by a form (referenced with `#include`) are specified
in the [yaml file format](https://learnxinyminutes.com/docs/yaml/).

Most data structures require a specific postfix be appended to their names in
order for their types to be specified, like `this-things-name-type`, which can
then be referenced by name without the type, like `{this-things-name}` using
python's string formatting notation. Naming otherwise follows the standard
syntax used in yaml, accessing each nested layer with dot notation or array
indices.

While data structures can be defined in terms of other structures, care must be
taken to avoid introducing a self-referential expression, as infinite recursion
is not currently guarded against.

The names `dice`, `self` and `@` are reserved internal names (discussed later)
that may not be defined by the user; attempting to define them will cause the
form to be aborted.


## Tables
[Simple Lists](#simple-lists)
[Weighted Items](#weighted-items)
[Countable Items](#countable-items)
[Conditional Items](#conditional-items)
[Selection Probability](#selection-probability)
[Multi-Valued Items](#multi-valued-items)
[Methods, Attributes and Indexing](#methods-attributes-and-indexing)

While not the most basic data structure that can be created, it is likely to be
the most common - tables are a group of items to make one (or more) selections
from. Tables can be as simple as a plain list of items, all equally likely, or
have the relative weight of each item specified or be composed of items that
can be removed from the collection during the selection process.

To create a table, its name must use the `-table` type suffix, for example
`random-stuff-table`. There are multiple ways to specify a table, depending on
how it should behave:


### Simple Lists
The simplest tables are just lists of equally likely items. For example, the
following would produce one of the words 'first', 'second' and 'third' each
with a probability of 1/3.

```yaml
simple-table: ['first', 'second', 'third']
```

Or, equivalently
```yaml
simple-table:
  - first
  - second
  - third
```



### Weighted Items
Relative weights can be specified for table items with a slightly different
syntax, where the table name is enclosed in square brackets, and the items and
weights are given in `item: weight` pairs. Weights can be numeric values or
string formatting expressions that evaluate to numeric values.

```yaml
weighted-table:
  first: 3
  second: 2
  third: 1
  wildcard: '{@[d4]}'
```

Weighted items can also be expressed slightly more verbosely, with `value: name`
and `count: expression` pairs in a sequence of mappings syntax. This is
necessary when including other item properties or multi-valued items:

```yaml
weighted-table:
- value: first
  weight: 3
- value: second
  weight: 2
- value: third
  weight: 1
- value: wildcard
  weight: '{@[d4]}'
```

In this syntax, if a weight is not specified, it defaults to 1.


### Countable Items
Some table methods will not only select an item from the table, but remove it as
well, so that it cannot be selected again. In some tables, it may be desirable
to allow an item to be selected more than once before it is removed. This could
be achieved by explicitly creating the duplicates, or each type of item in the
table can be specified once, with `value: 'name'` and `count: expression` pairs
in a sequence of mappings:

```yaml
pouch-table:
- value: gold
  count: 2
- value: silver
  count: 3
- value: copper
  count: '{@[10d10]}'
```

If a count is not specified, it defaults to 1. Otherwise, any numeric value or
string formatting expressions that evaluate to numeric values are allowed,
though counts are always rounded to the nearest integer and formatting
expressions are evaluated once and then remain fixed until the table is
`reset`.

When items are `taken` from a table, their counts are decremented until they are
less-than-or-equal-to zero, at which point the item cannot be selected again.


### Conditional Items
Table items can be activated/deactivated based on the evaluation of a python
code expression, using the `condition: 'code'` property. String formatting
syntax will be evaluated, which means values from previous selections and data
structures can be used in the test. Conditions will be re-evaluated for each
selection from the table, so that as the oerall state of the form evolves,
so do the available items in the table.

However, care must be taken to construct a valid python expression, in particular
putting quotes around string formatting expressions that produce a string, as
it will not be done automatically.

**conditional-table.yaml**
```yaml
letter-table: ['A', 'B', 'C']

# only one of 1, 2 or 3 can be selected based on the previously selected letter
number-table:
- value: 1
  condition: '"{letter.value}" == "A"'
- value: 2
  condition: '"{letter.value}" == "B"'
- value: 3
  condition: '"{letter.value}" == "C"'
# 4 can always be selected
- value: 4
```

**conditional.form**
```
{letter}: {number}{number}{number}{number}
```

**output**
```
C: 3343
```


### Selection Probability
The weight, count and condition of every item in a table contributes to its
probability of being chosen:

```math
p(item_i)=\frac{count_i*weight_i}{\sum_{0<=j<n} count_j*weight_j}
```

Those items whose conditions evaluate to `False` will not be included in the
calculation, effectively having `weight: 0`.

Furthermore, when the count of an item changes, the selection probabilities of
all items in that table will also change. If this is not desirable, the count
of table items may be ignored when calculating their overall weight by setting
the `no-count-weight: 1` property on those items (not all items in the table
need share this property).

```yaml
pouch-table:
  # ...
- value: pebble
  count: 5
  no-count-weight: 1
```


### Multi-Valued Items
Table items may be a list of values. In the simple case, where the items are
equally likely (weight=1, count=1), the table can be expressed as a nested list.
However, if any item must specify a property, the entire table must be expressed
as a sequence of mappings.

Each list item should be the same length to avoid index-out-of-bounds errors
when accessing it later, and note that negative indices are not supported by
python's string formatting.

**multivalued-table.yaml**
```yaml
multivalued-table:
- - A
  - B
- - X
  - Y

multiweighted-table:
- value: ['A', 'B']
  weight: 10
- value: ['x', 'Y']
  weight: 1
```

**multivalued.form**
```
#include multivalued-table.yaml
{multivalued.select[0]} {multivalued.value[1]}
{multiweighted.select[0]} {multiweighted.value[1]}
```


### Methods, Properties and Indexing
- `table.select` - Explicitly generate another item from the table. This is
equivalent to simply evaluating the table name itself, but can be indexed in
the case of multivalued table items.
- `table.take` - Generate an item from the table and remove its row from the
table; it supports indexing in the case of multivalued table items. It is an
error to attempt to take an item from an empty table.
- `table.selectmany[count,"sep"]` - As `table.select`, but multiple items are
generated from the table and joined together with a separator string. If the
separator is not specified, the string `', '` is used by default.
- `table.takemany[count,"sep"]` - As `table.take`, but multiple items are
generated from the table and joined together with a separator string. If the
separator is not specified, the string `', '` is used by default. It is an
error to take more items from the table than are in it.
- `table.value` - Get the previously generated result from the table; it
supports indexing in the case of multivalued table items. This attribute will
evaluate to an empty string until an item is first generated from the table.
- `table.selected` - A fully functional child table which has items added to
it as they are selected from the parent table. The items are inserted with
weight=1. This attribute evaluates to an empty string until an item is first
generated from the table.
- `table.reset` - Undo the effects of .take and reset the table to its initial
state, including the .value and .selected attributes. This attribute will
evaluate to an empty string.
- `table.noshow` - Select an item from the table, but return an empty string;
this can be used for initialization.
- `table[index]` - Retrieve an original item from the table, at the specified
index, treating the table instead as if it were a list and multivalued items
may be indexed further. This operation is not affected by the .take method.


## Accumulators
Accumulators take the selections made in tables and group and count them, for
example, turning output from a table, like this
```
copper coin
silver coin
gold coin
copper coin
silver coin
silver coin
```

into this
```
2 of copper coin; 3 of silver coin; 1 of gold coin
```

Accumulators can gather selections from one or more tables; they are specified
by using the `-accumulator` suffix and providing a list of table names, which
may include interpolotion strings.
```yaml
coins-table:
  cp: 100
  sp: 10
  gp: 1

jems-table:
  quartz: 99
  diamond: 1

pouch-accumulator: ['coins', 'jems']
```

Evaluating accumulators will not cause selections to be made from their source
tables, they will read `table.selected[]` from each - selections must be made
separately. Since it is unlinkely that the raw selections will want to be shown
as well as the accumulated ones, `table.noshow` or `table.selectmany[].noshow`
can be used to prime the tables for accumulators.
```
{coins.selectmany[100].noshow}{jems.selectmany[5].noshow}
{pouch}
```

```
89 of cp; 9 of sp; 2 of gp; 5 of quartz
```

### Extended Formatting Syntax
Accumulators, by default, will output items in the form `count of item` and
delimit each with `; `. The standard formatting syntax has been extended for
accumulators to allow one to specify not just the overall formatting of the
output (the standard), but also to specify how the elements are output, the
separator between each and a string to bookend the left and right sides in
the form `{accumulator-name:outer-spec:left:sep:right:item-spec}`, with each
field delimited with a colon. Formatting is applied to an accumulator by the
following procedure:

1. Apply `item-spec` to each item name and count pair
2. Join the formatted items with `sep` between each of them
3. Bookend the string with `left` and `right`
4. Apply `outer-spec` to the string

The `item-spec` field is a fully fledged formatting spec (though with several
differences, to stop the formatting algorithm from breaking) and each item name
and count are provided to it as positional arguments. This formatting spec
differs from a standard one in that it may not include curly braces; instead,
use parentheses. To include actual parentheses in the formatted string, escape
them with a backslash.

Several other characters are escaped:
* Colons in the left, sep or right fields must be escaped to differentiate them
from the delimitors between the fields. Colons in the item-spec may be used as
normal and do not need to be escaped.
* Line feed, carriage return and tab characters can be used with \n, \r and \t.
* Backslashes that are not part of an escape sequence themselves must be
espcaped with a backslash.

The default formatting spec, then, looks like this:
`{pouch:::; ::*:(1) of (0)}`

To change the format to something that looks like a table:
`{pouch:::\n::*:(0:<16)  (1:>4)}`

```
cp                  89
sp                   9
gp                   2
quartz               5
```

This extended syntax is only currently supported for accumulators.


## Fields
Fields contain constant values that do not change, unless they are defined as a
interpolation expression, in which case the interpolation is evaluated and
becomes the new, constant value of the field. Fields are the sole exception to
the rule that all data attributes require a type suffix - any attribute without
a known type is a field.


### Field Properties and Methods
- `field.name` - Get the name of this object, the final part of its path.
- `field.noshow` - Generate the value of the field, but display an empty
string. Can be used for initialization.


## Macros
Similar to fields, except when a macro contains an interpolation expression, it
is not fixed when it is first evaluated and instead will be re-interpolated the
next time it is evaluated, alowing one to create shorthand expressions. Macros
are defined with the `-macro` suffix.


**macros-and-fields.yaml**
```yaml
stat-macro: '{@[3d6]}'

str: '{stat}'
dex: '{stat}'
con: '{stat}'
int: '{stat}'
wis: '{stat}'
cha: '{stat}'

attack-macro: '{@[d20+{str}]}'
```


### Macro Properties and Methods
- `macro.many[count,"sep"]` - Output the macro multiple times, joined together
with a separator string. If the separator is not specified, the string `', '`
is used by default.
- `macro.noshow` - Generate the value of the macro, but display an empty
string. Can be used for initialization.
- `macro.value` - Return the current value in the object without generating a
new one.

### Macros and `self`
When used in a macro, self will refer to the object that the macro is being
invoked from, a table item, for example.


## Required Fields
Fields may be declared in an unspecified state in a form with the expectation
that they be specified via the command line. Such fields are declared with the
`-required` suffix. If a required field is not defined via the command line,
the user will be warned of each such field, along with the message the field
was defined with and the form will not be printed.

**coords.yaml**
```yaml
coords:
  x-required: the x coordinate
  y-required: the y coordinate
```

**coords.form**
```
#include coords.yaml
<{coords.x},{coords.y}>
```

**command line**
```
> python coords.form coords.x=3

ERROR: the following requirements have not been fulfilled:
  coords.y: the y coordinate

> python coords.form coords.x=3 coords.y=1
<3,1>
```


## States
States are similar to macros in that their values change each time they are
evaluated, but have a memory allowing them access their previous value which
persists across invocations of table.py. States can be constructed by using
the `-state` suffix and defining an `initial` value attribute and a `value`
attribute that is re-evaluated each time the state is. States can be reset
using the `.reset` method on the state.

State objects can reference themselves to get their current value (without
changing it) with `self.value` and can be reset with the `.reset` method.

The value of every state in the form is saved when execution is completed to
a file with the same path and name as the form with the `.state` extension
appended to it.


### State Properties and Methods
- `state.name` - Get the name of this object, the final part of its path.
- `state.noshow` - Generate the next value of the state, but display an empty
string. Can be used for initialization.
- `state.reset` - Reset the value of the state to the value specified as its
`initial` property.
- `state.value` - Return the current value in the object without generating a
new one.


### State Example
A simple form that rolls a d6 and adds it to the previous d6 that was rolled.

**roller.yaml**
```yaml
roller-state:
  initial: '{@[d6]}'
  value: '{@[d6+{self.value}]}'
```

**roller.form**
```
{roller} {roller} {roller} {roller}{roller.reset}
{roller} {roller} {roller} {roller}
```

**run 1**
```
> python tables.py roller.form
3 4 6 12
2 6 8 13

```

**run 2**
```
> python tables.py roller.form
16 20 21 23
2 6 9 10
```


## References to Other Tables and Data Structures
Other tables and non-table data structures can be referenced in table
definitions using the same notation as used in forms; non-table data structures
may be useful in the case of constants that are used more than once.

**THERE IS NO GUARD AGAINST INFINITE RECURSION** - proceed with care.

**references.yaml**
```yaml
large: [
  'This is annoyingly long',
  'Too long to have to type over and over']

small: 'tiny'
long-table: ['{large[0]}', '{large[1]}']
short-table: ['quick', 'fast', '{small}']
nested-table: ['{short}', '{long}']
```

# Dice
**Deprecation Notice**: `dice[]` is deprecated and replaced with the eval
object `@[]`; the syntax is otherwise the same. If dice[] is used, a warning
will be emitted on stderr. Until the `dice` object is removed, it remains a
reserved name and will cause an error if a user defines an object with that
name.

The top level name `@` is reserved for invoking the dice roller and expression
interpretation engine, and cannot defined in any user yaml files. It can be
invoked in string format expressions like other data structures with `@[expr]`.
The dice expression may contain other string format expressions, but currently
does not support spaces in the expression.

| Operator or Function | Syntax | Description |
| ----------- | ----------- | ----------- |
| **Grouping Symbols** | | |
| `()` | `(expr)` | parentheses, expression grouping |
| `{}` | `{format}` | curly braces, string formatting |
| **Dice Operators** | | |
| `D` `d` | `xDy` | binary dice roll, roll x y-sided dice; operands must be integers |
| `D` `d` | `Dy` | unary dice roll, roll one y-sided die; operands must be integers |
| `K` `k` | `xDyKz` | keep the z largest rolls (or smallest if z is negative); operands must be integers |
| `!` | `xDy!z` | explosion, roll another y-sided die if any result equals z; operands must be integers |
| `!<` | `xDy!<z` | explode if less than, roll another y-sided die if any result is less than z; operands must be integers |
| `!>` | `xDy!>z` | explode if greater than, roll another y-sided die if any result is greater than z; operands must be integers |
| `!p` | `xDy!pz`| penetrating explosion, roll another y-sided die and subtract one if any result equals z; operands must be integers |
| `!p<` | `xDy!p<z` | penetrating explode if less than, roll another y-sided die if any result is less than z; operands must be integers |
| `!p>` | `xDy!p>z` | penetrating explode if greater than, roll another y-sided die if any result is greater than z; operands must be integers |
| **Arithmetic Operators** | | |
| `+` | `x+y` | addition |
| `+` | `+x` | unary positive |
| `-` | `x-y` | subtraction |
| `-` | `-x` | unary negation |
| `*` | `x*y` | multiplication |
| `/` | `x/y` | real division |
| `//` | `x//y` | integer division |
| `%` | `x%y` | modulus, the remainder of x/y |
| `**` | `x**y` | exponentiation, x to the power y |
| **Comparison Operators** | | |
| `<` | `x<y` | less than |
| `<=` | `x<=y` | less than or equal |
| `>` | `x>y` | greater than |
| `>` | `x>=y` | greater than or equal |
| `==` | `x==y` | equal to |
| `!=` | `x!=y` | not equal to |
| **Boolean Logical Operators** | | |
| `~` | `~x` | not |
| `&` | `x&y` | and |
| `^` | `x^y` | exclusive or |
| <code>&#124;</code> | <code>x&#124;y</code> | or |
| **Functions** | | |
| `,` | `x,y` | argument separator |
| `if` | `if(test,x,y)` | if the test evaluates to true, return x, else y |
| `floor` | `floor(x)` | return the floor of exression x |
| `ceil` | `ceil(x)` | return the ceiling of expression x |


## Common Dice Roll Expressions
| Description | Expression |
| ----------- | ----------- |
| DnD advantage roll | `2D20K1` |
| DnD disadvantage roll | `2D20K-1` |
| DnD stats | `4D6K3` |


## Chaining Dice Roll Operators
Because the dice roll operator `D` has a unary form, it can be chained with
itself to perform more exotic dice rolls; when chained in this way, the
expression is evaluated from right to left, because the rightmost argument
defines the number of sides on the dice to roll. The `K` operator can also be
used multiple times to remove the least and greatest results from a roll to
select from the middle of the list.


| Description | Grouped Expression | Simplified Expression |
| ----------- | ----------- | ----------- |
| Roll a D10 and then roll 5 dice with that number of sides | `5D(D10)` | `5DD10` |
| Roll 2 D10s and then roll 5 dice with that number of sides | `5D(2D10)` | `5D2D10` |
| Roll 5 D2s and then roll that number of D10s | `(5D2)D10` | |
| Roll D10 distributed with an exponential decay | | `DDDDD10` |
| Roll 3D10 and keep the middle result | `(3D10K2)K-1` | `3D10K2K-1` |

## Example
**character.yaml**
```yaml
# tables are string only
level-table: ['1', '2', '3']
classes-table: ['fighter', 'wizard']
hitdie-fighter: 10
hitdie-wizard: 6
```

**character.form**
```
#include character.yaml
Class: {classes}
Level: {level}
Hit Dice: {level.value}D{hitdie-{classes.value}}
Hit Points: {@[{level.value}D{hitdie-{classes.value}}]}
```

**output**
```
Class: fighter
Level: 2
Hit Dice: 2D10
Hit Points: 11
```


# Markov Chains
Markov chains are lists of strings, specified in the
[yaml format](https://learnxinyminutes.com/docs/yaml/), whose names must have
the `-markov` suffix. Markov chains can be referenced in a form by using their
fully qualified name, except for the suffix that indicates its type.

For example, generate 4 new names from the top 10 boys and girls names in 2018
according to the Social Security Administration.

**names.yaml**
```yaml
names-markov: [
  'Liam',     'Emma',
  'Noah',     'Olivia',
  'William',  'Ava',
  'James',    'Isabella',
  'Oliver',   'Sophia',
  'Benjamin', 'Charlotte',
  'Elijah',   'Mia',
  'Lucas',    'Amelia',
  'Mason',    'Harper',
  'Logan',    'Evelyn']
```

**names.form**
```
#include names.yaml
{names}, {names}, {names} and {names}
```

**output**
```
Even, Is, Lia, and Lucaha
```
