import subprocess
import sys
import os


def run_in_pipenv(script):
    """Run the script in the pipenv shell."""
    if os.getenv('VIRTUAL_ENV'):
        subprocess.run(['python', script, *sys.argv[1:]])
    else:
        subprocess.run(['pipenv', 'run', 'python', script, *sys.argv[1:]])
