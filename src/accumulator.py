from src.base import Base
from src.formatting import recursive_format, iter_format, int_or_float
from src.table import Table

from collections import Counter
import re


def object_lookup(data, path):
    """Find a nested object using a string representation of it."""
    idx_re = re.compile(r'^(.+)\[([-+]?\d+)\]$')

    # keep a copy of the original path for debug and exceptions
    original_path = path[:]

    # first, interpolate and split the path on dots
    path = recursive_format(path, **data).split('.')
    obj = data

    # walk the path, handle indexes alng the way
    for attr in path:
        # look for and resolve indices
        indices = []
        while True:
            # check the nack of the expression for an index
            match = idx_re.search(attr)
            if not match:
                break

            # split the attr from the index we just found
            attr = match.group(1)
            indices.append(int(match.group(2)))

        # indices should only be on the back, if there are any brackets left,
        # the path is malformed
        if '[' in attr or ']' in attr:
            raise Exception(
                'Malformed indices found in {}'
                .format(original_path))

        # now that the attribute name has all indices stripped off, resolve it
        obj = getattr(obj, attr)

        # resolve indices. this is done in reverse order since the last one was
        # pushed into the list first, while the first index to resolve is at
        # the end of the list
        for idx in reversed(indices):
            obj = obj[idx]

    return obj


class AccumulatorCounter(Counter):
    """Wrapper for Counter to apply extending formatting."""

    def __format__(self, spec):
        """Apply extended iterable formatting."""
        items = ((item, count) for item, count in self.items() if count != 0)
        return iter_format(spec, self, items, '::; ::*:(1) of (0)')


class Accumulator(Base):
    """Accumulate previously selected items from a collection of Tables."""

    def __init__(self, sources, path, data):
        """
        Constructor.

        sources - A single table name or a list of table names to collect items
            from. String formatting is applied to these names when this object
            is formatted itself.
        """
        Base.__init__(self, 'accumulator', path)

        if isinstance(sources, str):
            self._sources = [sources]
        elif (isinstance(sources, (list, tuple))
                and all(isinstance(source, str) for source in sources)):
            self._sources = sources
        else:
            raise Exception(
                'Accumulator sources must a single string or list of strings,')

        # TODO: push to Base
        self._data = data
        self._current = None

    def _accumulate(self):
        """
        Resolve table names and count all selected items.

        If an item is list itself, the first field is taken to be the name and
        the rest are taken as counts and accumulated (TODO: only .
        """
        sources = [
            object_lookup(self._data, source)
            for source in self._sources]

        assert all(isinstance(source, Table) for source in sources)

        items = AccumulatorCounter()
        for source in sources:
            # anything pulled out of Table.selected has already had recursive
            # formatting applied to it, so we do not apply it again
            for item in source.selected:
                if isinstance(item, list):
                    if len(item) == 0:
                        self._log('tried to accumulate a zero-length list')
                        continue
                    elif len(item) == 1:
                        item, count = item[0], 1
                    else:
                        # TODO: accumulate counts beyond index=1
                        item, count = item[0], int_or_float(item[1])
                else:
                    count = 1

                items[item] += count

        self._current = items

    def __getitem__(self, key):
        """Return the counts for an item, accumulating them if needed."""
        return self.current[key]

    @property
    def current(self):
        """Return the current accumulation, or generate it if needed."""
        if self._current is None:
            self._accumulate()

        return self._current

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return '<accumulator {!s}>'.format(self._path)

    def __format__(self, spec):
        """Return the string representation used in forms."""
        # NOTE: recursive formatting is not applied since it would already have
        #   been when items were generated from tables.
        self._accumulate()
        return self._current.__format__(spec)
