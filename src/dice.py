import math
import random
import re

from src.debug import debug, warn


def sign(x, zero=0):
    """Return the sign of a number or an optional value if it was zero."""
    if x > 0:
        return 1
    elif x < 0:
        return -1
    else:
        return zero


def xor(a, b):
    """Return boolean xor."""
    return (a and not b) or (not a and b)


def roll_single_die(sides):
    """Roll a single die with the specified number of sides."""
    return random.randint(1, sides)


class DiceRoll:
    """Generate and perform operations on dice rolls."""

    def __init__(self, count, sides, rolls=None):
        """
        Constructor.

        Parameters
        ----------
        count - Number of dice to roll.
        sides - The number of sides on each die.
        rolls - optional - If specified, these values are used rather than
            generating them and the count argument is ignored and replaced by
            the length of this list.
        """
        debug('ctor')

        if (not isinstance(count, (int, DiceRoll))
                or not isinstance(sides, (int, DiceRoll))):
            raise Exception('Die roll operands must be ints or die rolls')

        self.count = count
        self.sides = sides

        if rolls is not None:
            self.rolls = sorted(rolls, reverse=True)
            self.count = len(rolls)

        else:
            self.rolls = sorted((
                roll_single_die(abs(sides))
                for _ in range(abs(count))),
                reverse=True)

        self.total = None
        self.explode_filters = []
        self.prev_explode_oper = 'br'

        # None - not initialized, no explosion operator encountered yet
        # e - normal explosion
        # p - penetrating explosion
        # c - compounding explosion TODO
        self.explode_type = None

    def get_total(self):
        """Return the dice result; calculate it if necessary."""
        debug('get_total')
        if self.total is None:
            self.finish_explosion()
            self.total = sign(self.count * self.sides) * sum(self.rolls)

        return self.total

    def __getitem__(self, index):
        """Return the results at the specified index in a new DiceRoll."""
        rolls = self.rolls[index]
        return DiceRoll(len(rolls), self.sides, rolls)

    def __neg__(self):
        """Return the negative of the total."""
        return -self.get_total()

    def __pos__(self):
        """Return the positive of the total."""
        return +self.get_total()

    def __abs__(self):
        """Return the absolute value of the total."""
        return abs(self.get_total())

    def _binary(self, other, f):
        """Perform a binary operation on a number or DiceRoll object."""
        if isinstance(other, DiceRoll):
            return f(self.get_total(), other.get_total())
        else:
            return f(self.get_total(), other)

    def __add__(self, other):
        """Return the sum of the totals."""
        return self._binary(other, lambda x, y: x + y)

    def __radd__(self, other):
        """Return the sum of the totals."""
        return other + self.get_total()

    def __sub__(self, other):
        """Return the difference of the totals."""
        return self._binary(other, lambda x, y: x - y)

    def __rsub__(self, other):
        """Return the difference of the totals."""
        return other + -self.get_total()

    def __mul__(self, other):
        """Return the product of the totals."""
        return self._binary(other, lambda x, y: x * y)

    def __rmul__(self, other):
        """Return the product of the totals."""
        return other * self.get_total()

    def __truediv__(self, other):
        """Return the dividend of the totals."""
        return self._binary(other, lambda x, y: x / y)

    def __rtruediv__(self, other):
        """Return the dividend of the totals."""
        return other / self.get_total()

    def __floordiv__(self, other):
        """Return the floored dividend of the totals."""
        return self._binary(other, lambda x, y: x // y)

    def __rfloordiv__(self, other):
        """Return the floored dividend of the totals."""
        return other // self.get_total()

    def __mod__(self, other):
        """Return the modulo of the totals."""
        return self._binary(other, lambda x, y: x % y)

    def __rmod__(self, other):
        """Return the modulo of the totals."""
        return other % self.get_total()

    def __pow__(self, other):
        """Return the exponentiation of the totals."""
        return self._binary(other, lambda x, y: x ** y)

    def __rpow__(self, other):
        """Return the exponentiation of the totals."""
        return other ** self.get_total()

    def __eq__(self, other):
        """Return whether the totals are equal or not."""
        return self._binary(other, lambda x, y: x == y)

    def __req__(self, other):
        """Return whether the totals are equal or not."""
        return other == self.get_total()

    def __ne__(self, other):
        """Return whether the totals are equal or not."""
        return self._binary(other, lambda x, y: x != y)

    def __rne__(self, other):
        """Return whether the totals are equal or not."""
        return other != self.get_total()

    def __lt__(self, other):
        """Return whether this total is less than the other."""
        return self._binary(other, lambda x, y: x < y)

    def __rlt__(self, other):
        """Return whether other is less than this total."""
        return other < self.get_total()

    def __le__(self, other):
        """Return whether this total is less than or equal to the other."""
        return self._binary(other, lambda x, y: x <= y)

    def __rle__(self, other):
        """Return whether other is less than or equal to this total."""
        return other <= self.get_total()

    def __gt__(self, other):
        """Return whether this total is greater than the other."""
        return self._binary(other, lambda x, y: x > y)

    def __rgt__(self, other):
        """Return whether other is greater than this total."""
        return other > self.get_total()

    def __ge__(self, other):
        """Return whether this total is greater than or equal to the other."""
        return self._binary(other, lambda x, y: x >= y)

    def __rge__(self, other):
        """Return whether other is greater than or equal to this total."""
        return other >= self.get_total()

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return the string representation of the roll with all rolls."""
        return (
            '<diceroll: {!s}, {!s}, {!s}>'
            .format(self.count, self.sides, self.rolls))

    def __format__(self, spec):
        """Return a string representaion of the total."""
        return self.get_total().__format__(spec)

    def explode(self, kind, oper, arg):
        """
        Apply an explosion operator to the die roll.

        The operator is applied lazily so that chains of explosion operators
        can be correctly applied.
        """
        if not isinstance(arg, (int, DiceRoll)):
            raise Exception('Explosion operands must be ints or die rolls')

        assert oper in ('eq', 'lt', 'gt')
        debug('exploding: {!r} {!r}', oper, arg)

        # make sure the explosion type did not change, no mixing-and-matching
        if self.explode_type is None:
            self.explode_type = kind
        elif self.explode_type != kind:
            raise Exception('mixing-and-matching of explosion types illegal')

        # get the function corresponding to the oper value
        if oper == 'eq':
            f = lambda x: x == arg
        elif oper == 'lt':
            f = lambda x: x < arg
        elif oper == 'gt':
            f = lambda x: x > arg

        if oper == 'eq' and arg == 0:
            oper = 'br'

        if self.prev_explode_oper == 'br' or oper == 'br':
            if oper != 'br':
                self.explode_filters.append([f])

        # neither operator is a break from here on
        elif self.prev_explode_oper == 'eq' or oper == 'eq':
            self.explode_filters.append([f])

        # neither operator is equals from here on
        elif self.prev_explode_oper == 'lt' and oper == 'gt':
            self.explode_filters.append([f])

        # all other operator chains are 'and' conditions, add to same chain
        else:
            self.explode_filters[-1].append(f)

        self.prev_explode_oper = oper
        debug('{}', len(self.explode_filters))
        return self

    def can_explode(self, x):
        """Check filters to determine if the value would explode or not."""
        return any(
            bool(f) and all(f0(x) for f0 in f)
            for f in self.explode_filters)

    def finish_explosion(self):
        """Actually explode the dice rolls."""
        # short-circuit if there's nothing to explode
        if not self.explode_filters:
            debug('no explosions')
            return

        assert self.explode_type is not None

        # check to see if this roll will explode infinitely because all of the
        # filters are satisfied for all possible die values
        if all(self.can_explode(x) for x in range(1, abs(self.sides) + 1)):
            debug('infinite explosion')
            self.rolls.append(float('inf'))
            self.count += sign(self.count, 1)
            return

        # explosion will stop for some non-zero probability
        # TODO: put a threshold on the number of times a die can explode
        debug('completing explosion')

        # track which rolls need adjusted and do it after all explosions so
        # that we don't have to keep adjusting rolls on the fly to tell whether
        # they should explode or not. the rolls we already have are the raw
        # results from the initial roll, and won't be adjusted.
        adj = [0] * len(self.rolls)

        # explosion type e normal: 0
        # explosion type p penetrating: -1
        adj0 = 0
        if self.explode_type == 'p':
            adj0 = -1

        idx = 0
        while idx < len(self.rolls):
            if self.can_explode(self.rolls[idx]):
                self.rolls.append(roll_single_die(abs(self.sides)))
                self.count += sign(self.count, 1)
                adj.append(adj0)

            idx += 1

        # TODO: should this be sorted?
        self.rolls = [a + b for a, b in zip(self.rolls, adj)]


def roll_dice(count, sides):
    """Return a list of random die rolls."""
    return DiceRoll(count, sides)


def keep_dice(dice, count):
    """Return a new DiceRoll with a subset of the dice."""
    assert isinstance(dice, DiceRoll)

    if not isinstance(count, int):
        raise Exception('Right keep operand must be an int')

    if count >= 0:
        return dice[:count]
    else:
        return dice[count:]


def explode_dice(dice_roll, kind, oper, arg):
    """Apply an explosion operator to a DiceRoll."""
    assert isinstance(dice_roll, DiceRoll)
    return dice_roll.explode(kind, oper, arg)


class Operator:
    """Container class to represent operators."""

    def __init__(self, token, num_args, associativity, precedence, function):
        """Constructor."""
        self.token = token
        self.n = num_args
        self.associativity = associativity
        self.precedence = precedence
        self.f = function

    def __call__(self, *args):
        """Evaluate the operator with the given arguments."""
        assert len(args) == self.n
        return self.f(*args)

    def __lt__(self, other):
        """Compare operator precedence for postfix creation."""
        assert isinstance(other, Operator)

        result = (
            self.precedence < other.precedence
            or self.precedence == other.precedence
            and self.associativity == 'L')

        debug(
            'compare: {:2} {:4} {}  <  {:2} {:4} {}  ?  {}',
            self.token,  self.precedence,  self.associativity,
            other.token, other.precedence, other.associativity,
            result)

        return result


class Function:
    """Container class to represent functions."""

    def __init__(self, token, num_args, function):
        """Constructor."""
        self.token = token
        self.n = num_args
        self.f = function

    def __call__(self, *args):
        """Evaluate the function with the given arguments."""
        assert len(args) == self.n
        return self.f(*args)


operators = {
    # unary opeators must all be of the same precedence
    'ud': Operator('ud', 1, 'R', 1000, lambda a: roll_dice(1, a)),
    'u-': Operator('u-', 1, 'R', 1000, lambda a: -a),
    'u+': Operator('u+', 1, 'R', 1000, lambda a: a),
    'u~': Operator('u~', 1, 'R', 1000, lambda a: not a),

    # binary operators
    # dice operators
    'd':  Operator('d',  2, 'R', 320, lambda a, b: roll_dice(a, b)),
    'k':  Operator('k',  2, 'L', 310, lambda a, b: keep_dice(a, b)),
    # exploding, coumpounding and penetrating exploding dice
    # TODO: these need left unary implementations
    '!':  Operator(
        '!',  2, 'L', 300, lambda a, b: explode_dice(a, 'e', 'eq', b)),
    '!>': Operator(
        '!>', 2, 'L', 300, lambda a, b: explode_dice(a, 'e', 'gt', b)),
    '!<': Operator(
        '!<', 2, 'L', 300, lambda a, b: explode_dice(a, 'e', 'lt', b)),
    # compounding is used for dice pools. the sum of the resulting rolls is the
    # same as normal explosion, but the dice that explode are grouped together
    # with their explosions, it's largely a matter of how to present the result
    # 5d6!  = 2+4+(6+2)+3+(6+6+1)
    # 5d6!! = 2+4+(8)+3+(13)
    # '!!': Operator(
    #   '!!', 2, 'L', 300, lambda a, b: explode_dice(a, 'c', 'eq', b)),
    '!p':  Operator(
        '!p',  2, 'L', 300, lambda a, b: explode_dice(a, 'p', 'eq', b)),
    '!p>': Operator(
        '!p>', 2, 'L', 300, lambda a, b: explode_dice(a, 'p', 'gt', b)),
    '!p<': Operator(
        '!p<', 2, 'L', 300, lambda a, b: explode_dice(a, 'p', 'lt', b)),

    # arithmetic operators
    '**': Operator('**', 2, 'R', 230, lambda a, b: a ** b),
    '*':  Operator('*',  2, 'L', 220, lambda a, b: a * b),
    '/':  Operator('/',  2, 'L', 220, lambda a, b: a / b),
    '//': Operator('//', 2, 'L', 220, lambda a, b: a // b),
    '%':  Operator('%',  2, 'L', 220, lambda a, b: a % b),
    '+':  Operator('+',  2, 'L', 210, lambda a, b: a + b),
    '-':  Operator('-',  2, 'L', 210, lambda a, b: a - b),

    # relational operators
    '<':  Operator('<',  2, 'L', 130, lambda a, b: a < b),
    '<=': Operator('<=', 2, 'L', 130, lambda a, b: a <= b),
    '>':  Operator('>',  2, 'L', 130, lambda a, b: a > b),
    '>=': Operator('>=', 2, 'L', 130, lambda a, b: a >= b),
    '==': Operator('==', 2, 'L', 120, lambda a, b: a == b),
    '!=': Operator('!=', 2, 'L', 120, lambda a, b: a != b),

    # logical operators
    '&':  Operator('&',  2, 'L', 112, lambda a, b: a and b),
    '^':  Operator('^',  2, 'L', 111, lambda a, b: xor(a, b)),
    '|':  Operator('|',  2, 'L', 110, lambda a, b: a or b),

    # sequence operators
    # this operator is never evaluated, only used for conversion from infix
    # to postfix. the reason being is that both arguments would need to be
    # returned to the stack instead of producing a single result from them.
    # instead, we will just remove them from the postfix, since functions know
    # how many arguments they will consume.
    ',':  Operator(',',  0, 'L', 000, None), }


functions = {
    'if':    Function('if', 3, lambda a, b, c: b if a else c),
    'floor': Function('floor', 1, math.floor),
    'ceil':  Function('ceil', 1, math.ceil), }


def is_operator(token):
    """Determine if a token is an operator or not."""
    return token in operators


def is_function(token):
    """Determine if a token is a function name or not."""
    return token in functions


def is_operand(token):
    """Determine if a token is an operand or not."""
    return (
        token not in operators
        and token not in functions
        and token != '(' and token != ')')


def split_atoms(expr, atoms=None):
    """Split a string expression into a list of atoms."""
    # operators and functions will be matched using the pipe, which is not
    # greedy, but will match patterns left to right, so operators and functions
    # that the same initial character sequences must appear before thier
    # shorter counterparts, so functions and operators are sorted in reverse
    # order

    # remove 'u' from the beginning of internal unary synbols
    operator_set = {
        oper[1:] if oper[0] == 'u' else oper
        for oper in operators}

    atom_splitter_re = '|'.join(
        [re.escape(oper) for oper in reversed(sorted(operator_set))]
        + [re.escape(paren) for paren in '()']
        + [func for func in reversed(sorted(functions))]
        + [r'[0-9]*\.?[0-9]+'])

    atom_splitter_re = re.compile('^({})(.*)$'.format(atom_splitter_re))
    int_re = re.compile(r'^\d+$')
    float_re = re.compile(r'[0-9]*\.[0-9]+')

    # convert to lower so we don't need to bother with case
    expr = expr.lower()

    atoms = []
    while expr:
        # split the next atom from the rest of the string
        match = atom_splitter_re.search(expr)
        if not match:
            raise Exception('rest of expression bad: ' + expr)

        atoms.append(match.group(1))
        expr = match.group(2)

        # look for operands and unary operators
        if int_re.match(atoms[-1]):
            atoms[-1] = int(atoms[-1])
        elif float_re.match(atoms[-1]):
            atoms[-1] = float(atoms[-1])
        else:
            assert (
                is_operator(atoms[-1])
                or is_operator('u' + atoms[-1])
                or is_function(atoms[-1])
                or atoms[-1] in '()')

            # check to see if the operator has a unary form
            if is_operator('u' + atoms[-1]):
                # check to see if this is a unary operator
                if (len(atoms) == 1
                        or is_operator(atoms[-2])
                        or atoms[-2] == '('):
                    atoms[-1] = 'u' + atoms[-1]

    return atoms


def infix_to_postfix(infix):
    """Convert an infix expression to a postfix expression."""
    # https://en.wikipedia.org/wiki/Shunting-yard_algorithm
    infix = ['('] + infix + [')']
    postfix = []
    stack = []

    debug('<-----')
    debug('infix: {}', infix)
    debug('stack: {}', stack)
    debug('postfix: {}', postfix)

    for token in infix:
        debug('------')
        if is_function(token):
            debug('function: {}', token)
            stack.append(token)

        elif is_operator(token):
            debug('operator: {}', token)
            while stack[-1] != '(' and (
                    is_function(stack[-1])
                    or operators[token] < operators[stack[-1]]):
                postfix.append(stack.pop())
            stack.append(token)
            debug('stack: {}', stack)
            debug('postfix: {}', postfix)

        elif token == '(':
            debug('left paren')
            stack.append(token)
            debug('stack: {}', stack)
            debug('postfix: {}', postfix)

        elif token == ')':
            debug('right paren')
            while stack[-1] != '(':
                postfix.append(stack.pop())
            if stack[-1] == '(':
                stack.pop()
            debug('stack: {}', stack)
            debug('postfix: {}', postfix)

        else:
            debug('operand: {}', token)
            postfix.append(token)
            debug('stack: {}', stack)
            debug('postfix: {}', postfix)

    debug('----->')

    while stack:
        postfix.append(stack.pop())

    # remove sequence operators from function parameter lists
    postfix = [token for token in postfix if token != ',']

    debug('postfix: {}', postfix)
    return postfix


def evaluate_postfix(postfix):
    """Evaluate a postfix expression to a single, numeric value."""
    stack = []
    for token in postfix:
        if is_operator(token) or is_function(token):
            if is_operator(token):
                f = operators[token]
            else:
                f = functions[token]

            # make sure there are enough operands in the stack for the operator
            assert len(stack) >= f.n
            operands = [*stack[-f.n:]]

            # clear the operands from the stack
            del stack[-f.n:]

            # evaluate and put the new result onto the stack
            # use !r repr formatting to avoid triggering finalazation of
            # expressions
            debug('evaluate: {!r}, {!r}', token, operands)
            stack.append(f(*operands))
            debug('result: {!r}', stack[-1])
        else:
            stack.append(token)

    # there should only be a single, numeric value left in the stack, the
    # result of fully evaluating the expression
    assert len(stack) == 1
    assert isinstance(stack[-1], (int, float, DiceRoll))
    return stack[-1]


class Dice:
    """Dice expression roller and interpreter."""

    def __init__(self, deprecated=False):
        """
        Constructor.

        TODO: Remove after deprecated dice[] object is removed.
        """
        self.deprecated = deprecated

    def __getitem__(self, expr):
        """Evaluate a dice expression, dice[expr]."""
        if self.deprecated:
            warn(
                '"dice[expr]" is deprecated and will be removed in the '
                'future - use "@[expr]" instead')

        debug('dice expression: {}', expr)

        atoms = split_atoms(expr)
        debug('infix atoms: {}', atoms)

        atoms = infix_to_postfix(atoms)
        debug('postfix atoms: {}', atoms)

        result = evaluate_postfix(atoms)
        debug('result: {}', result)

        return result
