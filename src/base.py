from src.debug import log


class Base:
    """Base class for form structures."""

    def __init__(self, module, path):
        """
        Constructor.

        Parameters
        ----------
        module - The name of the module that the derived object belongs to.
            Used for log message filtering.
        path - The dot-delimited path to the object from the form.
        """
        self._module = module
        self._path = path

    def _log(self, msg, *args, **kwargs):
        """Send a message to the log, incorporating the module and path."""
        msg = self._path + ': ' + msg
        log(self._module, msg, *args, **kwargs)

    @property
    def noshow(self):
        """Trigger formatting of the object, but output an empty string."""
        self.__format__('')
        return ''
