from src.base import Base
from src.formatting import recursive_format


class State(Base):
    """Class to hold variable formatting strings with remembered values."""

    def __init__(self, name, obj, current, path, data):
        """
        Constructor.

        Parameters
        ----------
        name - The name of the field, can be accessed in format string via
            self.name.
        obj - A dict defining the initial and subsequent values of the state.
            'value' - The value to return when a string representation is
                requested. If it is a string itself, fomatting will be
                attempted which willreplace this value.
            'initial' - The starting value for the state and the value that
                will be set if the state is reset.
            'temp' - Optional. If set, the state will not persist across runs.
        current - The current initial value of the state, unless set to None.
            However, when reset is called, the state will take on the value of
            the initial argument.
        path - The dot-delimited path to the state from the form.
        data - Struct containing arguments to use for formatting.
        """
        if (not isinstance(obj, dict)
                or 'initial' not in obj
                or not isinstance(obj['initial'], str)
                or 'value' not in obj
                or not isinstance(obj['value'], str)):
            raise Exception(
                '{} was declared as state, which must be a dict '
                'with initial and value keys, which must have '
                'string values.'.format(path))

        Base.__init__(self, 'state', path)

        self._name = name
        self._value = obj['value']
        self._initial = obj['initial']
        self._temp = obj.get('temp', False)
        self._data = data
        self._initialized = False
        self._loading = current is not None
        self._current = current if current is not None else ''

        self._log(
            'constructed: value={} initial={} current={}',
            name, self._value, self._initial, current)

    def _initialize(self):
        """Reset to the initial state."""
        self._initialized = True

        # we determine if we're loading based on whether the state was built
        # with a current value or not. if we are loading, just reset the flag
        # and stop, the self._current was already set in the constructor.
        if self._loading:
            self._loading = False
            return

        # otherwise, we are building a fresh value from self._initial, which
        # might be an expression that needs evaluated
        self._data.self.push(self)
        self._current = recursive_format(self._initial, **self._data)
        self._data.self.pop()

    def _next(self):
        """Iterate to the next state, returning the current state."""
        # we may need other data to initialize the state, so we wait until
        # a value is requested to do it to ensure everything is loaded.
        if not self._initialized:
            not_loading = not self._loading
            self._initialize()

            # if we aren't loading, the next value will be the initial value
            if not_loading:
                return self._current

        # otherwise, we need to create the next state value
        self._data.self.push(self)
        self._current = recursive_format(self._value, **self._data)
        self._data.self.pop()

        return self._current

    @property
    def reset(self):
        """Reset to the initial state and iterate to the next."""
        self._loading = False
        self._initialized = False
        return self._next()

    @property
    def name(self):
        """Return the name of the state."""
        return self._name

    @property
    def value(self):
        """Return the current value of the state."""
        return self._current

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return '<state {!s}: {!s}>'.format(self._path, self._current)

    def __format__(self, spec):
        """Return the string representation used in forms."""
        return self._next().__format__(spec)
