from src.base import Base
from src.formatting import recursive_format


class Field(Base):
    """Class to hold variable formatting strings until fixed at eval time."""

    def __init__(self, name, value, path, data):
        """
        Constructor.

        Parameters
        ----------
        name - The name of the field, can be accessed in format string via
            self.name.
        value - The value to return when a string representation is requested.
            If it is a string itself, fomatting will be attempted which will
            replace this value.
        path - The dot-delimited path to the field from the form.
        data - Struct containing arguments to use for formatting.
        """
        Base.__init__(self, 'field', path)

        self._name = name
        self._value = value
        self._data = data
        self._done = False

        if not isinstance(self._value, str):
            self._current = self._value

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return (
            '<field {!s}: {!s}>'
            .format(self._path, self._value))

    @property
    def name(self):
        """Return the name of the field."""
        return self._name

    def __format__(self, spec):
        """
        Return a string representation of the value.

        If the value is a string, formatting is performed and the result
        replaces the original value.
        """
        self._log('format')

        if not self._done:
            self._done = True
            if isinstance(self._value, str):
                self._data.self.push(self)
                self._value = recursive_format(self._value, **self._data)
                self._data.self.pop()

        result = self._value.__format__(spec)

        return result
