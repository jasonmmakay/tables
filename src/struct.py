from collections.abc import Mapping


class struct(Mapping):
    """Mapping using c-struct notation."""

    def __init__(self, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        **kwargs - initialize attributes with the names of the keywords and
            with thier corresponding values.
        """
        self.__dict__.update(**kwargs)

    def __getattr__(self, k):
        """Get the value of an attribute."""
        return self.__dict__[k]

    def __setattr__(self, k, v):
        """Set the value of an attribute."""
        self.__dict__[k] = v

    def __delattr__(self, k):
        """Remove an attribute."""
        del self.__dict__[k]

    def __getitem__(self, k):
        """Use bracket notation to get the value of an attribute."""
        return self.__dict__[k]

    def __setitem__(self, k, v):
        """Use bracket notation to set the value of an attribute."""
        self.__dict__[k] = v

    def __delitem__(self, k):
        """Use bracket notation to delete an attribute."""
        del self.__dict__[k]

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return the string representation of all attributes."""
        return str(self.__dict__)

    def __iter__(self):
        """Iterate over the attribute names."""
        yield from self.__dict__

    def __len__(self):
        """Return the number of attributes."""
        return len(self.__dict__)
