from src.base import Base
from src.formatting import iter_format

import re


_re_obj = re.compile(r'^\s*(\d+)\s*(?:,\s*"(.*)"\s*)?$')


def convert_escapes(x):
    """Convert escape sequnces in a string to the correct character."""
    return (
        x.encode('utf8')
        .decode('unicode_escape')
        .encode('latin1')
        .decode('utf8'))


class ManySelector(Base):
    """Wrapper to select one or more items from an object."""

    def __init__(self, obj, name, *args, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        obj - The object to select items from, which must have a _select
            method, though it may take any type of parameters.
        name - The name of the object to display in messages.
        *args, **kwargs - Parameters for obj._select().
        """
        Base.__init__(self, obj._module, obj._path + '.' + name)

        self._obj = obj
        self._args = args
        self._kwargs = kwargs
        self._count = None

    def __getitem__(self, count):
        """
        Modify the selector to choose multiple items.

        Parameters
        ----------
        count - The number of items to select.

        """
        self._log('getting selection list')
        if self._count is not None:
            raise Exception('{} already selected'.format(self._path))

        if not isinstance(count, int):
            raise Exception('{} count must be an int'.format(self._path))

        self._count = count
        return self

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return (
            '<{!s}: {!s}>'
            .format(self._path, self._count))

    def __format__(self, spec):
        """Select items from the table and format them."""
        if self._count is None:
            raise Exception('{} must provide a count'.format(self._path))

        items = tuple(
            self._obj._select(*self._args, **self._kwargs)
            for _ in range(self._count))

        return iter_format(spec, items, items, '::, ::*:(0)')
