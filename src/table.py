from src.base import Base
from src.formatting import recursive_format, int_or_float, parse_bool
from src.many import ManySelector

import random


def is_str_item(item):
    """Test if a raw table item is a simple string."""
    return isinstance(item, str)


def is_list_item(item):
    """Test if a raw table item is a list of strings."""
    return (
        isinstance(item, list)
        and all(isinstance(subitem, str) for subitem in item))


_required_item_properties = ('value',)


_optional_item_properties = (
    'weight', 'condition', 'count', 'no-count-weight',)


_all_item_properties = _required_item_properties + _optional_item_properties


def is_dict_item(item):
    """Test if a raw table item is a dict with the correct keys."""
    return (
        isinstance(item, dict)
        and all(key in item for key in _required_item_properties)
        and all(key in _all_item_properties for key in item)
        and isinstance(item['value'], str)
        and isinstance(item.get('condition', ''), str))


def is_dict_list_item(item):
    """Test if a raw table item is a dict with the correct keys."""
    return (
        isinstance(item, dict)
        and all(key in item for key in _required_item_properties)
        and all(key in _all_item_properties for key in item)
        and isinstance(item['value'], list)
        and all(isinstance(subitem, str) for subitem in item['value'])
        and isinstance(item.get('condition', ''), str))


def convert_escapes(x):
    """Convert escape sequnces in a string to the correct character."""
    return (
        x.encode('utf8')
        .decode('unicode_escape')
        .encode('latin1')
        .decode('utf8'))


class TableItem:
    """Contains all information for a single row from a table."""

    def __init__(
            self, value, count=1, weight=1,
            condition='True', no_count_weight='False'):
        """
        Constructor.

        Parameters
        ----------
        count - the number of times this item can be taken from a table before
            finally being removed.
        value - either a string or list of strings which may contain references
            to other objects that will be dereferenced via string formatting.
        weight - the relative weight against all other items in the same table
            that governs the probability of selection.
        condition - string containing a valid python expression which will be
            evaluated at selection time to determine whether this item may be
            selected or not; may include references to other objects that will
            be dereferenced via string formatting.
        no_count_weight - if set, do not apply count to item weight.
        """
        if isinstance(value, str):
            self._type = 0
            self._size = 0
        elif isinstance(value, list):
            self._type = 1
            self._size = len(value)
        # TODO: elif isinstance(value, dict):
        else:
            raise Exception(
                "Table items must be a str or list, got {}"
                .format(type(value)))

        self._value = value
        self._count = count
        self._weight = weight
        self._condition = condition
        self._no_count_weight = parse_bool(no_count_weight)

    def copy(self):
        """Create a copy of this table item."""
        return TableItem(
            self._value, self._count, self._weight, self._condition,
            self._no_count_weight)

    def initialize(self, data):
        """Convert count and weight properties from strings."""
        if isinstance(self._count, str):
            self._count = round(float(recursive_format(self._count, **data)))
        else:
            self._count = round(self._count)

        if isinstance(self._weight, str):
            self._weight = int_or_float(recursive_format(self._weight, **data))

    def is_active(self, data):
        """Evaluate the condition wih respect to global data."""
        return eval(recursive_format(self._condition, **data))

    @property
    def weight(self):
        """Return the weight for selecting this item from a table."""
        if self._no_count_weight:
            return self._weight
        else:
            return self._count * self._weight

    @property
    def count(self):
        """The current count of this item."""
        return self._count

    def take(self):
        """Decrement count; return True when less-than-or-equal to zero."""
        self._count -= 1
        return self._count <= 0

    def eval(self, data):
        """Evaluate all string formatting and return the result."""
        if self._type == 0:
            return recursive_format(self._value, **data)
        elif self._type == 1:
            return [
                recursive_format(subitem, **data)
                for subitem in self._value]


class Table(Base):
    """Container to generate random items for formatted strings."""

    def __init__(self, items, path, data):
        """
        Constructor.

        Parameters
        ----------
        items - Table items must be a mapping of strings to numbers or a list
            of one of the following types: string, list of strings, a mapping
            with a "value" key and a string or list of strings value.
        path - The dot-delimited path to the table from the form.
        data - struct containing all loaded data structures.

        """
        Base.__init__(self, 'table', path)

        self._data = data
        self._items = []
        self._item_size = None  # 0=str, 1+=list
        self._original_items = []
        self._previous = ''
        self._selected = None
        self._initialized = False

        if isinstance(items, list):
            for item in items:
                self._add(item)

        elif isinstance(items, dict):
            for item, weight in items.items():
                self._add(dict(value=item, weight=weight))

        else:
            raise Exception(
                'table items must be a mapping of strings to numbers or a '
                'list of one of the following types: string, list of strings, '
                'a mapping with a "value" key and a string or list of strings '
                'value')

    def _add(self, item):
        """Add items to the table."""
        if is_str_item(item) or is_list_item(item):
            item = TableItem(item)

        elif is_dict_item(item) or is_dict_list_item(item):
            item = TableItem(
                item['value'],
                item.get('count', 1),
                item.get('weight', 1),
                item.get('condition', 'True'),
                item.get('no-count-weight', False))
        else:
            raise Exception(
                'table items must be a string, list of strings, a mapping '
                'with a "value" key and a string or list of strings value or '
                'a string to number mapping. got {} in {}'
                .format(type(item), self._path))

        if self._original_items:
            if item._type != self._original_items[0]._type:
                raise Exception('all table items must be of the same type')
            if item._size != self._original_items[0]._size:
                raise Exception('all table items must be of the same length')

        self._items.append(item)
        self._original_items.append(item.copy())

    @property
    def select(self):
        """Return an item from the table."""
        return self._select()

    @property
    def take(self):
        """Remove and return an item from the table."""
        return self._select(remove=True)

    @property
    def selectmany(self):
        """Return a delimited list of items from the table."""
        return ManySelector(self, 'selectmany')

    @property
    def takemany(self):
        """Remove and return a delimited list of items from the table."""
        return ManySelector(self, 'takemany', remove=True)

    @property
    def reset(self):
        """Revert the table to its original state."""
        return self._reset()

    @property
    def value(self):
        """Return the last selected value."""
        # TODO: change to .value
        return self._previous

    @property
    def selected(self):
        """Get the child table containing all previous selections."""
        if self._selected is None:
            return ''
        else:
            return self._selected

    def __getitem__(self, index):
        """Return an item from the original table, by index."""
        if not isinstance(index, int):
            raise Exception('Indexes into table must be integers')

        return self._original_items[index]._value

    def __iter__(self):
        """Iterate over the items currently in the table."""
        return (item._value for item in self._items)

    def _select(self, remove=False):
        """
        Return an item from the table.

        The probability of an item being selected is relatively weighted given
        all other items. For an item to be selected, its condition attribute
        must also evaluate to true.

        The selected item will be the new table.value value and it will be
        added to table.selected. If table.take was called, it will be removed
        from the table so that it cannot be selected again.

        """
        self._log('selecting')
        if not self._items:
            raise Exception('Table is empty')

        # initialize all item weights and counts, which may initialize other
        # tables if they are referenced for them
        if not self._initialized:
            self._initialized = True
            for item in self._items:
                item.initialize(self._data)

        # find all items whose conditions are satisfied
        items = [
            (idx, item) for idx, item in enumerate(self._items)
            if item.is_active(self._data)]

        self._log(
            'items available: {}/{}/{}',
            len(items),
            len(self._items),
            len(self._original_items))

        if not items:
            raise Exception('Conditions have inactivated all items')

        # select an item by weight
        active_weight = sum(item.weight for idx, item in items)
        total_weight = sum(item.weight for item in self._items)
        w = random.uniform(0, active_weight)
        self._log('weight: {}/{}/{}', w, active_weight, total_weight)

        selected_idx = -1
        for idx, item in items:
            if w < item.weight:
                selected_idx = idx
                break
            else:
                w -= item.weight

        self._log('selected index: {}', selected_idx)
        item = self._items[selected_idx]

        # remove the item from the table if .take was called
        if remove and item.take():
            self._items.pop(selected_idx)

        # do string formatting now to lock down values to save for access via
        # .value, otherwise, the value might change later if the unformatted
        # result were returned
        item = item.eval(self._data)

        # save the formatted item for later access with .value
        self._previous = item

        # and add it to the child .selected table
        if self._selected is None:
            self._selected = Table([], self._path + '.selected', self._data)
        self._selected._add(item)

        self._log('selected item: {}', item)
        return item

    def _reset(self):
        """Revert the table to its original state."""
        self._initialized = False
        self._items = [item.copy() for item in self._original_items]
        self._previous = ''
        del self._selected
        self._selected = None
        return ''

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return '<table {!s}>'.format(self._path)

    def __format__(self, spec):
        """Implicitly call table.select when table is used in format string."""
        return self._select().__format__(spec)
