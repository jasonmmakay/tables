from src.base import Base
from src.debug import warn

from collections import Counter
from fractions import Fraction
import pickle
import random


class MetaStart(type):
    """Metaclass to print a Start object."""

    def __str__(self):
        """Return '<'."""
        return '<'

    def __repr__(self):
        """Return '<Start>'."""
        return '<Start>'


class Start(metaclass=MetaStart):
    """
    Placeholder for start of sequences for markov chains.

    This class is meant to be used much like None, without instances.
    """

    pass


class MetaEnd(type):
    """Metaclass to print an End object."""

    def __str__(self):
        """Return '>'."""
        return '>'

    def __repr__(self):
        """Return '<End>'."""
        return '<End>'


class End(metaclass=MetaEnd):
    """
    Placeholder for end of sequences for markov chains.

    This class is meant to be used much like None, without instances.
    """

    pass


def zip_sub_sequences(sequence, order):
    """Create subsequnces for training a markov chain."""
    # pad the front and back of the sequence with Start, End and None symbols
    # depending on the order of the chain
    sequence = (
        *([Start] * order),
        *sequence,
        *([End] * order),
        None)

    # generate the first subsequences
    first_sub_sequences = (
        sequence[idx:idx-order]  # noqa:E226
        for idx in range(order))

    # the notation used to generate the first subsequences isn't valid for the
    # final subsequence, which runs to the end of the padded sequence
    return zip(*first_sub_sequences, sequence[order:])


class MarkovChain(Base):
    """Generate random strings based on seed strings."""

    # versions with significant structural changes that require special
    # handling to upgrade
    _version_back_chain = 4

    # version number of this implementation of MarkovChain
    # IMPORTANT: this must be incremented whenever the class changes
    _version = 5

    def __init__(self, sequences, path, order=1, weight=1):
        """
        Constructor.

        Count the tranisitions between characters from a list of strings.

        Parameters
        ----------
        sequences - a list of strings.
        path - the data path to the object.
        order - the number of past symbols to consider when choosing the next
            symbol during sequence generation; default=1.
        weight - the amount of weight to apply for all transitions in the
            training sequences; default=1.
        """
        Base.__init__(self, 'markov', path)
        self.version = MarkovChain._version
        self.order = order
        self.chains = dict()
        self.back_chains = dict()
        self.add(sequences, weight)

    @staticmethod
    def upgrade(obj, upgrade_warning=False):
        """
        Upgrade a MarkovChain to the latest version, if it isn't already.

        Parameters
        ----------
        obj - The MarkovChain to upgrade.
        upgrade_warning - Whether or not to display a warning if an upgrade
            occurs; default=False.
        """
        assert isinstance(obj, MarkovChain)

        version = -1
        if hasattr(obj, 'version'):
            version = obj.version

        if version < MarkovChain._version:
            if upgrade_warning:
                warn(
                    'MarkovChain {} is being upgraded from version {} to {}',
                    obj._path, version, MarkovChain._version)

            upgraded = MarkovChain([], obj._path, obj.order)
            upgraded.chains = obj.chains

            if version < MarkovChain._version_back_chain:
                for sequence, transitions in upgraded.chains.items():
                    for symbol, weight in transitions.items():
                        back_sequence = (*sequence[1:], symbol)

                        back_chain = upgraded.back_chains.get(back_sequence)
                        if back_chain is None:
                            back_chain = Counter()
                            upgraded.back_chains[back_sequence] = back_chain

                        back_chain[sequence[0]] = weight
            else:
                upgraded.back_chains = obj.back_chains

            return upgraded

        else:
            return obj

    @staticmethod
    def load(file_name, path=None, upgrade_warning=True):
        """
        Load a pickled MarkovChain and upgrade it if needed.

        Parameters
        ----------
        file_name - The file to load the object from.
        path - Overwrite the path in the loaded object.
        upgrade_warning - Whether or not to display a warning if an upgrade
            occurs; default=True.
        """
        with open(file_name, 'rb') as f:
            obj = pickle.load(f)
            assert isinstance(obj, MarkovChain)
            if path is not None:
                obj._path = path

            return MarkovChain.upgrade(obj, upgrade_warning)

    def save(self, file_name):
        """
        Pickle the MarkovChain.

        Parameters
        ----------
        file_name - File to write the object to.
        """
        with open(file_name, 'wb') as f:
            pickle.dump(self, f)

    def odds(self, sequence):
        """
        Return the probability of generating each element of the sequence.

        Returns
        -------
        Iterator of Fraction objects.
        """
        for s in zip_sub_sequences(sequence, self.order):
            s = (*s,)
            a, b = s[:-1], s[-1]

            if b is End:
                break

            options = self.chains.get(a, {})
            n = options.get(b, 0)
            d = sum(options.values())
            if d == 0:
                d = 1

            yield Fraction(n, d)

    def add(self, sequences, weight=1):
        """
        Add sequences to the chain.

        sequences - a list of strings.
        path - the data path to the object.
        order - the number of past symbols to consider when choosing the next
            symbol during sequence generation; default=1.
        weight - the amount of weight to apply for all transitions in the
            training sequences; default=1.
        """
        assert isinstance(weight, int)
        assert all(
            isinstance(sequence, str)
            for sequence in sequences)

        order = self.order
        chains = self.chains
        back_chains = self.back_chains

        for sequence in sequences:
            self._log(
                'adding sequence="{sequence}" weight={weight}',
                sequence=sequence,
                weight=weight)

            for s in zip_sub_sequences(sequence, order):
                s = (*s,)

                a, b = s[:-1], s[-1]
                a_chain = chains.get(a)

                if a_chain is None:
                    a_chain = Counter()
                    chains[a] = a_chain

                a_chain[b] += weight

                a, b = s[0], s[1:]
                b_chain = back_chains.get(b)

                if b_chain is None:
                    b_chain = Counter()
                    back_chains[b] = b_chain

                b_chain[a] += weight

    def _generate(self, seed, direction, no_break_first):
        """
        Helper function for generate().

        PARAMETERS
        ----------
        seed - Base to generate the rest of the sequence off of. Must be equal
            in length to the order of the chain.
        direction - -1 or 1, generate backward from the front of the seed or
            forward from the back of it.
        no_break_first - Do not select a transition to a Start or an End symbol
            on the first transition, if possible.
        """
        assert direction in (-1, 1)
        result = [seed]

        if direction == 1:
            chains = self.chains
        else:
            chains = self.back_chains

        while True:
            if (direction == 1 and result[-1][0] is End
                    or direction == -1 and result[-1][-1] is Start):
                break

            transitions = chains[result[-1]]
            total = sum(transitions.values())

            if no_break_first:
                delta = transitions.get(Start, 0) + transitions.get(End, 0)
                if delta < total:
                    # subtract the weights of the Start and End transitions
                    total -= delta
                else:
                    # otherwise the only transitions are to Start or End, so we
                    # have to select one of them
                    print('only transition is break', *transitions.keys())
                    no_break_first = False

            n = random.randrange(total)
            for selection, n0 in transitions.items():
                # skip over Start or End transitions if configured to
                if no_break_first and (selection is Start or selection is End):
                    continue

                if n < n0:
                    if direction == 1:
                        self._log('{} => {}', result[-1], selection)
                        result.append((*result[-1][1:], selection))
                    else:
                        self._log('{} <= {}', selection, result[-1])
                        result.append((selection, *result[-1][:-1]))

                    break
                else:
                    n -= n0

            # if this was the first transition, we don't need to skip Start or
            # End transitions anymore
            no_break_first = False

        # if we generated backwards, the front of the sequence was the last
        # part that was generated
        if direction == -1:
            result = list(reversed(result))

        # the first order elements of result have the start symbol for the
        # first element, with the first actual generated symbol at order+1;
        # the last generated symbol will be the first element at index -2
        # and the last will be entirely composed of the end symbol
        return ''.join(r[0] for r in result[self.order:-1])

    def generate(self, front=None, middle=None, back=None):
        """Create a new string based on the transitions in the seeds."""
        assert len([x for x in (front, middle, back) if x is not None]) <= 1

        self._log('generating')
        order = self.order

        if front is not None:
            padding = (Start,) * (order - len(front))
            seed = (*padding, *front[-order:])
            result = front + self._generate(seed, 1, False)

        elif back is not None:
            padding = (End,) * (order - len(back))
            seed = (*back[:order], *padding)
            result = self._generate(seed, -1, False) + back

        elif middle is not None:
            assert len(middle) >= order
            front_seed = (*middle[:order],)
            back_seed = (*middle[-order:],)
            result = (
                self._generate(front_seed, -1, True)
                + middle
                + self._generate(back_seed, 1, True))

        else:
            seed = (Start,) * order
            result = self._generate(seed, 1, False)

        self._log('result = {}', result)
        return result

    @property
    def total(self):
        """Return the total number of observations in the chain."""
        return sum(
            sum(transitions.values())
            for transitions in self.chains.values())

    def count(self, seq):
        """
        Return the number of times an observation was in the chain.

        PARAMETERS
        ----------
        seq - The sequence to count observations of; it may either be of equal
            length to the order of the chain or one greater; all other cases
            return zero.
        """
        if len(seq) == self.order:
            transitions = self.chains.get(seq)

            if transitions is None:
                return 0

            return sum(transitions.values())

        elif len(seq) == self.order + 1:
            seq, seq_next = seq[:-1], seq[-1]
            transitions = self.chains.get(seq)

            if transitions is None:
                return 0

            return transitions.get(seq_next, 0)

        else:
            return 0

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return '<markov {!s}>'.format(self._path)

    def __format__(self, spec):
        """Generate and format a new string."""
        return self.generate().__format__(spec)
