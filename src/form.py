import os
import re

import toml
import yaml

from src.accumulator import Accumulator
from src.debug import log as base_log
from src.dice import Dice
from src.field import Field
from src.formatting import recursive_format
from src.macro import Macro
from src.markov import MarkovChain
from src.requirement import Requirement
from src.self import SelfStack
from src.state import State
from src.struct import struct
from src.table import Table


def log(msg, *args, **kwargs):
    """Write a log message for the form module."""
    base_log('form', msg, *args, **kwargs)


def int_float_str(x):
    """Attempt to convert a string to an int or float."""
    assert isinstance(x, str)

    try:
        return int(x)
    except Exception:
        pass

    try:
        return float(x)
    except Exception:
        pass

    return x


def parse_properties(data, properties):
    """
    Parse name=value property strings into nested dicts.

    Parameters
    ----------
    data - Nested dict to augment.
    properties - List of name=value strings.

    """
    property_re = re.compile(
        r'^([a-zA-Z0-9_-]+(?:\.[a-zA-Z0-9_-]+)*)=(.+?)$')

    for prop in properties:
        match = property_re.match(prop)
        if not match:
            raise Exception()

        path, value = match.group(1, 2)
        value = int_float_str(value)
        path = path.split('.')

        x = data
        for key in path[:-1]:
            if key not in x:
                x[key] = dict()
            elif not isinstance(x[key], dict):
                raise Exception()

            x = x[key]

        x[path[-1]] = value


class Path:
    """Dynamic path container."""

    def __init__(self):
        """Constructor."""
        self._path = []

    def append(self, part):
        """Add a part to the path."""
        self._path.append(part)

    def pop(self):
        """Remove the last added part of the path."""
        self._path.pop()

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Concatenate all pieces of the path with dots."""
        return '.'.join(self._path)


def add_to_parent(parent, obj, name):
    """
    Add an object to a parent list or struct.

    Parameters
    ----------
    parent - The list or struct to add the object to.
    obj - The object to add.
    name - The attribute to set on structs, ignored for lists.

    Returns
    -------
    Return the added object, useful for constructing it inline.

    """
    if isinstance(parent, struct):
        parent[name] = obj
    elif isinstance(parent, list):
        parent.append(obj)
    else:
        raise Exception('unhandled parent type {}'.format(type(parent)))

    return obj


def parse_name(name):
    """
    Parse a name for type suffixes.

    Parameters
    ----------
    name - The name to parse, searches for the following type suffixes:
        markov
        table
        state
        macro
        required

    Returns
    -------
    If the name has a type suffix, return the name with the suffix stripped
    off and the type string; else, return the name unchanged and None.

    """
    # TODO: turn the type names into an enum
    if name.endswith('-markov'):
        return name[:-len('-markov')], 'markov'
    elif name.endswith('-table'):
        return name[:-len('-table')], 'table'
    elif name.endswith('-state'):
        return name[:-len('-state')], 'state'
    elif name.endswith('-macro'):
        return name[:-len('-macro')], 'macro'
    elif name.endswith('-required'):
        return name[:-len('-required')], 'required'
    elif name.endswith('-accumulator'):
        return name[:-len('-accumulator')], 'accumulator'
    else:
        return name, None


def objects_from_dict(obj, state, roots):
    """
    Convert standard python data structure to structs, Tables and MarkovChains.

    Parameters
    ----------
    obj - The dict to convert.
    state - A dict containing state values loaded from file.
    roots - A dict mapping object paths to the directory it was loaded from.
    """
    if not isinstance(obj, dict):
        raise Exception('object must be dict')

    # this parses depth-first, path can be a list with each component pushed
    # on before and popped off after recursive calls of _objects_from_dict
    path = Path()
    root = struct()

    def _objects_from_dict(parent, obj, name, path_suffix, parent_type):
        # parse the object type from the name, if there is one
        name, obj_type = parse_name(name)
        if path_suffix is None:
            path_suffix = ''

        # some objects recurse on themselves and remove their original
        # name from the path so that the next call puts it back into the path
        # in which case, we should not pop when we finish
        pop_path_on_exit = True

        # add the name, and suffix for lists, to the object path
        path.append(name + path_suffix)
        log('parsing object {}: {}/{}', path, obj_type, type(obj))

        if obj_type == 'markov':
            # TODO: push this into Markov.__init__, like Table.__init__
            markov_chain = None

            # strings are paths to pickled MarkovChain objects
            if isinstance(obj, str):
                filename = os.path.join(roots[str(path)], obj)
                markov_chain = MarkovChain.load(filename, str(path))

            # lists of strings are sequences to train MarkovChains
            elif (isinstance(obj, list)
                    and all(isinstance(v, str) for v in obj)):
                markov_chain = MarkovChain(obj, str(path))

            else:
                raise Exception(
                    '{} was declared as a morkov chain, which must be '
                    'a list of only strings'.format(path))

            add_to_parent(parent, markov_chain, name)

        elif obj_type == 'table':
            add_to_parent(parent, Table(obj, str(path), root), name)

        elif obj_type == 'accumulator':
            add_to_parent(parent, Accumulator(obj, str(path), root), name)

        elif obj_type == 'required':
            # NOTE: first, the form is parsed, then command line properties
            # and then objects are converted. if a required property was
            # defined on the command line, it would have overwritten the
            # required field from the form and we would never have built a
            # Requirement object

            # pass str(path) to take a snapshot of the path since when this is
            # evaluated to raise the exception, path will have been emptied
            # after exiting the recursion
            add_to_parent(parent, Requirement(str(path), obj), name)

        # nested types
        elif (obj_type == 'macro'
                or obj_type == 'state'):
            # macro objects can have objects nested inside them, all of which
            # are interpreted as macros even if they are not declared to be,
            # but they cannot be declared to be something else
            if parent_type is not None and parent_type != obj_type:
                raise Exception(
                    '{} was declared a {}, but was declared a {} at a higher '
                    'level'.format(path, obj_type, parent_type))

            # pop the name off of the path, we're going to put it back on in
            # the next recursion, but we need to remember it's already popped
            path.pop()
            pop_path_on_exit = False
            _objects_from_dict(parent, obj, name, '-' + obj_type, obj_type)

        elif isinstance(obj, list):
            # parse objects nested in list objects
            child = add_to_parent(parent, list(), name)
            path.pop()
            pop_path_on_exit = False

            for idx, grand_child in enumerate(obj):
                # create a suffix to append to the path. we don't append here
                # because there may be a type suffix that won't be parsed from
                # the name until the next recusrsive call.
                idx = '[{}]'.format(idx)

                _objects_from_dict(
                    child,
                    grand_child,
                    name,
                    idx,
                    parent_type)

        elif isinstance(obj, dict) and parent_type != 'state':
            # parse objects nested in ordinary dict objects
            child = add_to_parent(parent, struct(), name)

            for grand_child_name, grand_child in obj.items():
                _objects_from_dict(
                    child,
                    grand_child,
                    grand_child_name,
                    None,
                    parent_type)

        else:
            # anything else is a field, macro or state; possibly nested
            if parent_type == 'macro':
                child = Macro(name, obj, str(path), root)
            elif parent_type == 'state':
                current = state.get(str(path))
                child = State(name, obj, current, str(path), root)
            else:
                assert parent_type is None
                child = Field(name, obj, str(path), root)

            add_to_parent(parent, child, name)

        # now that we've parsed the object, pop its name from the path
        if pop_path_on_exit:
            path.pop()

    # we ensured the base object was a dict above, start parsing the objects
    # nested in it
    for child_name, child_obj in obj.items():
        _objects_from_dict(root, child_obj, child_name, None, None)

    # root now contains all of the parsed objects
    return root


def get_object_paths(obj):
    """
    Return a list of all object paths.

    Parameters
    ----------
    obj - A dict containing the nested object to extract the paths from.
    """
    all_paths = []
    path = Path()

    def _get_object_paths(obj, name, path_suffix):
        # parse the object type from the name, if there is one
        name, obj_type = parse_name(name)
        if path_suffix is None:
            path_suffix = ''

        # some objects recurse on themselves and remove their original
        # name from the path so that the next call puts it back into the path
        # in which case, we should not pop when we finish
        pop_path_on_exit = True

        # add the name, and suffix for lists, to the object path
        path.append(name + path_suffix)

        if (obj_type == 'markov'
                or obj_type == 'table'
                or obj_type == 'accumulator'
                or obj_type == 'required'):
            all_paths.append(str(path))

        elif (obj_type == 'macro'
                or obj_type == 'state'):
            path.pop()
            pop_path_on_exit = False
            _get_object_paths(obj, name, '-' + obj_type)

        elif isinstance(obj, list):
            # parse objects nested in list objects
            path.pop()
            pop_path_on_exit = False

            for idx, grand_child in enumerate(obj):
                # create a suffix to append to the path. we don't append here
                # because there may be a type suffix that won't be parsed from
                # the name until the next recusrsive call.
                idx = '[{}]'.format(idx)
                _get_object_paths(grand_child, name, idx)

        elif isinstance(obj, dict):
            # parse objects nested in ordinary dict objects
            for grand_child_name, grand_child in obj.items():
                _get_object_paths(grand_child, grand_child_name, None)

        else:
            all_paths.append(str(path))

        # now that we've parsed the object, pop its name from the path
        if pop_path_on_exit:
            path.pop()

    # we ensured the base object was a dict above, start parsing the objects
    # nested in it
    for child_name, child_obj in obj.items():
        _get_object_paths(child_obj, child_name, None)

    return all_paths


def extract_states(obj, states):
    """Recursively extract state paths and values from an object."""
    if isinstance(obj, (list, tuple)):
        for child in obj:
            extract_states(child, states)

    elif isinstance(obj, (struct, dict)):
        for name in obj:
            extract_states(obj[name], states)

    elif isinstance(obj, State) and not obj._temp:
        states[obj._path] = obj._current


def extract_requirements(obj, requirements):
    """Recursively extract unfulfilled requirement paths from an object."""
    if isinstance(obj, (list, tuple)):
        for child in obj:
            extract_requirements(child, requirements)

    elif isinstance(obj, (struct, dict)):
        for name in obj:
            extract_requirements(obj[name], requirements)

    elif isinstance(obj, Requirement):
        requirements.add(obj)


class Form:
    """Loads form files and performs string formatting."""

    def __init__(self, filename, properties, reset):
        """Load the form and include data files."""
        self.data = None
        self.form = []
        self.filename = filename

        dirname = os.path.dirname(filename)

        include_filenames = []
        with open(filename) as f:
            for line in f:
                if line.startswith('#include '):
                    include_filenames.append(
                        os.path.join(dirname, line[len('#include'):].strip()))
                else:
                    self.form.append(line.rstrip())

        # load the form's state, if it exists
        state = {}
        if os.path.exists(filename + '.state') and not reset:
            state = toml.load(filename + '.state')

        self.data = {}
        roots = {}
        for filename in include_filenames:
            if filename.lower().endswith('.toml'):
                data = toml.load(filename)
            elif filename.lower().endswith('.yaml'):
                with open(filename, 'r') as yaml_file:
                    data = yaml.load(yaml_file, Loader=yaml.BaseLoader)
            else:
                raise Exception(
                    'Unable to determine file format from extension for {}. '
                    'Valid formats: toml, yaml.'
                    .format(filename))

            self.data.update(data)

            for path in get_object_paths(data):
                roots[path] = os.path.dirname(filename)

        parse_properties(self.data, properties)
        self.data = objects_from_dict(self.data, state, roots)

        if 'dice' in self.data:
            raise Exception(
                '"dice" is a reserved top-level name and cannot be defined '
                'by the user, but will be deprecated in the future')

        elif '@' in self.data:
            raise Exception(
                '"@" is a reserved top-level name and cannot be defined by '
                'the user')

        elif 'self' in self.data:
            raise Exception(
                '"self" is a reserved top-level name and cannot be defined '
                'by the user')

        self.data.dice = Dice(deprecated=True)
        self.data['@'] = Dice()
        self.data.self = SelfStack()

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return '<form {!s}>'.format(self.filename)

    def __format__(self, spec):
        """Format all lines in the from."""
        return '\n'.join(
            recursive_format(line, **self.data)
            for line in self.form)

    def __del__(self):
        """Save form state values to be used next time the form is loaded."""
        states = dict()
        extract_states(self.data, states)

        if not states:
            return

        with open(self.filename + '.state', 'w') as state_file:
            toml.dump(states, state_file)

    def get_requirements(self):
        """Get the path to any unfulfilled requirements."""
        requirements = set()
        extract_requirements(self.data, requirements)
        return requirements
