from src.debug import log

import re


def parse_bool(x):
    """Parse a string to a bool, unless the object is a bool already."""
    if isinstance(x, bool):
        return x
    if x.lower() == 'false':
        return False
    if x.lower() == 'true':
        return True
    if x == '0':
        return False
    if x == '1':
        return True

    raise Exception(
        'A bool string must be one of, case insensitive: 0, 1, false, true')


def int_or_float(x):
    """Convert a string to an int or float, depending on its contents."""
    try:
        return int(x)
    except Exception:
        pass

    try:
        return float(x)
    except Exception:
        raise Exception('string "{}" is not an int or float'.format(x))


def recursive_format(text, *args, **kwargs):
    """
    Recursively format strings.

    Allows for fields to be partially named and completed with the evaluation
    of another string formatting expression.

    Guaranteed not to modify text inline.

    Parameters
    ----------
    text - the text to apply formatting to.
    *args, **kwargs - arguments to pass to str.format().
    """
    # copy the string so the original is not overwritten
    original_text = text
    text = text[:]
    log('formatting', '"{}"', original_text)

    while True:
        # look for the innermost curly braces that do not have any others
        # inside them
        match = re.search(r'^(.*?)(\{[^\{\}]*\})(.*)$', text, re.S)

        # break the loop if there's nothing left to format
        if not match:
            break

        # apply formatting to the text within the curly braces
        try:
            inner = match.group(2).format(*args, **kwargs)
        except Exception as ex:
            print(
                'failed to format "{}" from original "{}"'
                .format(match.group(2), text))
            raise ex

        text = ''.join((
            match.group(1),
            inner,
            match.group(3)))

    log('formatting', '"{}" => "{}"', original_text, str(text))

    return text


def iter_format(spec, obj, it, default_rest):
    """
    Apply formatting to a container of objects.

    {obj:ospec}
    {obj:ospec:left:sep:right:unpack:ispec}
    """
    # placeholder for real colons that we want to split the spec on
    class colon:
        pass

    # copy the original spec
    original_spec = spec[:]

    # the escapes we handle here
    escapes = {
        r'\\':  '\\',
        r'\:':  ':',
        r':':   colon,
        r'\(':  '(',
        r'(':   '{',
        r'\)':  ')',
        r')':   '}',
        r'\n':  '\n',
        r'\r':  '\r',
        r'\t':  '\t'}

    # the maximum number of times to split on colons
    _MAX_SPEC_FIELDS = 6

    def split_spec(spec):
        # split on escaped escapes, escaped and unescaped colons and parens,
        # and the common whitespace escapes: \t \n \r.
        # NOTE: despite the docs saying that {{ and }} will escape the curlies,
        #   it only seems to work when they are paired, meaning you cannot
        #   escape just an opening curly, you have to have an escaped closing
        #   curly, too.
        spec = re.split(r'(\\\\|\\:|:|\\\(|\(|\\\)|\)|\\[nrt])', spec)

        # find and convert escapes
        spec = (escapes.get(subspec, subspec) for subspec in spec)

        # split the list of subspecs on colons
        groups = [[]]
        for subspec in spec:
            if subspec is colon:
                if len(groups) < _MAX_SPEC_FIELDS:
                    groups.append([])
                    continue

                subspec = ':'

            groups[-1].append(subspec)

        # join all of the parts of each group back together
        spec = [''.join(group) for group in groups]

        # now, there should either be one part to the spec, for a normal format
        # spec, or _MAX_SPEC_FIELDS, for the outer spec, left side, separator,
        # right side, unpack tpye, and item spec

        if len(spec) == 1:
            return spec

        if len(spec) != _MAX_SPEC_FIELDS:
            raise Exception(
                'iter spec must have 1 or {} fields, found {}'
                .format(_MAX_SPEC_FIELDS, len(spec)))

        if spec[4] not in ('', '*', '**'):
            raise Exception(
                'iter spec field 4, unpack type, must be empty, * or **')

        return spec

    spec = split_spec(original_spec)

    if len(spec) == 1:
        spec = split_spec(original_spec + default_rest)
        assert len(spec) == _MAX_SPEC_FIELDS

    outer_spec, front, sep, back, unpack, item_spec = spec

    if unpack == '':
        middle = sep.join(item_spec.format(item) for item in it)
    elif unpack == '*':
        middle = sep.join(item_spec.format(*item) for item in it)
    elif unpack == '**':
        middle = sep.join(item_spec.format(**item) for item in it)

    return ''.join([front, middle, back]).__format__(outer_spec)
