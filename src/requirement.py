class Requirement:
    """Placeholder for unspecified but mandatory form fields."""

    def __init__(self, path, msg):
        """
        Constructor.

        Parameters
        ----------
        path - String specifying the object location which will be emitted in
            an exception if this object is formatted into a string.
        msg - A message to be displayed with the object location.

        """
        self._path = path
        self._msg = msg

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation fo ruse in debug."""
        return '<requirement {!s}>'.format(self._path)

    def __format__(self, spec):
        """Raise an exception with the path to this object in it."""
        raise Exception(
            '{} is a required field but has not been defined: {}'
            .format(self._path, self._msg))
