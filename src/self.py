class SelfStack:
    """Hold objects that will be refered to as 'self' in format strings."""

    def __init__(self):
        """Constructor."""
        self._stack = []

    def push(self, obj):
        """Add a new 'self' for recursive calls."""
        self._stack.append(obj)

    def pop(self):
        """Remove the previous 'self' after recursive calls."""
        return self._stack.pop()

    def __getattr__(self, attr):
        """Pass attribute requests to the most recent 'self'."""
        if self._stack:
            return getattr(self._stack[-1], attr)
        else:
            raise Exception()

    def __str__(self):
        """Return string representation of all 'self' objects."""
        return '<self: {!s}>'.format(self._stack)

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __format__(self, spec):
        """Pass format requests to the most recent 'self'."""
        if self._stack:
            return self._stack[-1].__format__(spec)
        else:
            raise Exception()
