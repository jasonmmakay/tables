import os
from sys import stderr


__debug__enabled__ = False


def enable_debug():
    """Turn on application-wide debug messages."""
    global __debug__enabled__
    __debug__enabled__ = True


def is_debug_enabled():
    """Test if the application-wide debug flag is set."""
    global __debug__enabled__
    return __debug__enabled__


def debug(msg, *args, **kwargs):
    """
    Print a debug message, if debug is enabled.

    Parameters
    ----------
    msg - message to display.
    *args, **kwargs - arguments to format msg with.
    """
    if is_debug_enabled():
        print(
            '>>>', msg.format(*args, **kwargs),
            flush=True, file=stderr)


def debug_echo(x):
    """Print an obects type and itself, if debug is enabled, and return it."""
    if is_debug_enabled():
        print(
            '>>>', x.__class__.__name__, ':', x, '<<<',
            flush=True, file=stderr)

    return x


def echo(x):
    """Print an object, if debug is enabled, and return it."""
    print('>>>', x, '<<<', flush=True, file=stderr)
    return x


__warnings__ = set()


def warn(msg, *args, **kwargs):
    """
    Print a warning message, if it hasn't already been printed.

    Parameters
    ----------
    msg - message to display.
    *args, **kwargs - arguments to format msg with.
    """
    global __warnings__
    warning = msg.format(*args, **kwargs)

    if warning not in __warnings__:
        print('WARNING:', warning, flush=True, file=stderr)
        __warnings__.add(warning)


def error(msg, *args, **kwargs):
    """
    Print an error message.

    Parameters
    ----------
    msg - message to display.
    *args, **kwargs - arguments to format msg with.
    """
    global __warnings__
    err = msg.format(*args, **kwargs)
    print('ERROR:', err, flush=True, file=stderr)


__log_initialized__ = False
__log_modules__ = set()


def enable_log(*modules):
    """Enable logging of a module by name."""
    global __log_modules__
    __log_modules__.update(modules)


def disable_log():
    """Disable all logging enabled through enable_log()."""
    global __log_modules__
    __log_modules__.clear()


def log(module, msg, *args, **kwargs):
    """
    Print a message to log.txt.

    Parameters
    ----------
    module - the name of the module the message is from; the message is not
        output unless logging has been enabled for this module.
    msg - message to print to the log.
    *args, **kwargs - arguments to format msg with; all values are converted
        to strings with str() before being formatting msg with them.
    """
    global __log_initialized__
    global __log_modules__

    if module not in __log_modules__ and 'all' not in __log_modules__:
        return

    # delete the log file on the first message
    if not __log_initialized__ and os.path.exists('log.txt'):
        os.remove('log.txt')

    args = [str(v) for v in args]
    kwargs = {k: str(v) for k, v in kwargs.items()}

    with open('log.txt', 'a') as log:
        print(
            '{}:'.format(module),
            msg.format(*args, **kwargs),
            file=log)

    __log_initialized__ = True
