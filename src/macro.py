from src.base import Base
from src.formatting import recursive_format
from src.many import ManySelector


class Macro(Base):
    """Class to hold variable formatting strings that change with each eval."""

    def __init__(self, name, value, path, data):
        """
        Constructor.

        Parameters
        ----------
        name - The name of the field, can be accessed in format string via
            self.name.
        value - The value to return when a string representation is requested.
            If it is a string itself, fomatting will be attempted which will
            replace this value.
        path - The dot-delimited path to the field from the form.
        data - Struct containing arguments to use for formatting.
        """
        Base.__init__(self, 'macro', path)

        if not isinstance(value, str):
            raise Exception('macro values must be strings')

        self._name = name
        self._current = ''
        self._value = value
        self._data = data

    def _select(self):
        """Return the unformatted value for .many[] method."""
        return self

    @property
    def many(self):
        """Return a ManySelector wrapper object."""
        return ManySelector(self, 'many')

    @property
    def value(self):
        """Return the current value without generating a new one."""
        return self._current

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    def __str__(self):
        """Return a string representation for use in debug."""
        return (
            '<macro {!s}: {!s}>'
            .format(self._path, self._value))

    def __format__(self, spec):
        """Return the unformatted string representation of the value."""
        if isinstance(self._value, str):
            # DO NOT push self, it should refer to the previous object
            self._current = recursive_format(self._value, **self._data)
        else:
            self._current = self._value

        return self._current
